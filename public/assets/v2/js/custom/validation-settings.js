var validationObject = {
    errorElement: 'label',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
        if (element.parent().hasClass('input-group')) {
            error.appendTo(element.parent().parent());
        }
        else {
            error.appendTo(element.parent());
        }
    },

    highlight: function (input) {
        $(input).closest('.form-group').addClass('has-error');
    },

    unhighlight: function (input) {
        $(input).closest('.form-group').removeClass('has-error');
    }
};