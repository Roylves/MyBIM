function ajaxForm($elm) {
    var $btnElm = $elm.find('button[type=submit]');
    var btnLabel = $btnElm.val();

    $elm.ajaxForm({
        beforeSubmit: function() {
            $btnElm.prop('disabled', true);
            $btnElm.val('Loading...');
        },
        success: function(response, statusText, xhr, formElm) {
            window.location.reload();
        },
        error: function(response) {
            var $flashElm = $elm.find('.alert-danger');
            $flashElm.html(response.responseJSON.message);
            $flashElm.removeClass('hidden');
            $btnElm.prop('disabled', false);
            $btnElm.val(btnLabel);
        }
    });
}

