<?php
/*
 * jQuery File Upload Plugin PHP Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

error_reporting(E_ALL | E_STRICT);
require('UploadHandler.php');
$manufacturerCode = isset($_SERVER['HTTP_MANUFACTURERCODE'])?$_SERVER['HTTP_MANUFACTURERCODE']:$_REQUEST['manufacturerCode'];
if($_SERVER['HTTP_HOST'] == 'localhost'){
	$custom_dir = "/xampp/htdocs/cidb/cidb_assets/objects/$manufacturerCode/";
	$custom_url = "http://localhost/cidb/cidb_assets/objects/$manufacturerCode/";
}else{
	$custom_dir = $_SERVER['DOCUMENT_ROOT']."/cidb_assets/objects/$manufacturerCode/";
	$custom_url = "http://$_SERVER[HTTP_HOST]/cidb_assets/objects/$manufacturerCode/";
}
// echo $custom_dir;
$upload_handler = new UploadHandler(array('upload_dir' => $custom_dir, 'upload_url'=> $custom_url));