@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-3">
            @include('frontend.profile._sidebar')
        </div>

        <div class="col-lg-9">
            <br>
            <div class="row">
                <div class="col-lg-12 col-md-12 mb-12">
                    <div class="card">
                        <div class="card-header">
                            Ratings
                        </div>
                        <div class="card-block">
                            <div class="table-responsive">
                                <table class="table table small table-striped table-hover">
                                    <tbody>
                                        @foreach ($ratings as $review)
                                            <tr>
                                                <td>
                                                    {{ $review->created_at->diffForHumans() }}
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="{{ action('ObjectController@getBySlug', [$review->reviewrateable->manufacturer->slug, $review->reviewrateable->slug]) }}">
                                                            {{ $review->reviewrateable->code }} {{ $review->reviewrateable->name }}
                                                        </a>
                                                    </p>
                                                    <p>
                                                        @for ($i = 0; $i < $review->rating; $i++)
                                                            &#9734;
                                                        @endfor
                                                    </p>
                                                    <p>{{ $review->title }}</p>
                                                    <p>{{ $review->body }}</p>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                {{ $ratings->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        </div>
    </div>
@endsection