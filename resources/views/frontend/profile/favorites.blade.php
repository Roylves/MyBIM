@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-3">
            @include('frontend.profile._sidebar')
        </div>

        <div class="col-lg-9">
            <br>
            <div class="row">
                <div class="col-lg-12 col-md-12 mb-12">
                    <div class="card">
                        <div class="card-header">
                            Favorites
                        </div>
                        <div class="card-block">
                            <div class="table-responsive">
                                <table class="table table small table-striped table-hover">
                                    <tbody>
                                        @foreach ($favorites as $favorite)
                                            <tr>
                                                <td>
                                                    {{ $favorite->created_at->diffForHumans() }}
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="{{ action('ObjectController@getBySlug', [$favorite->favoriteable->manufacturer->slug, $favorite->favoriteable->slug]) }}">
                                                            {{ $favorite->favoriteable->code }} {{ $favorite->favoriteable->name }}
                                                        </a>
                                                    </p>
                                                    <p>
                                                        @for ($i = 0; $i < $favorite->rating; $i++)
                                                            &#9734;
                                                        @endfor
                                                    </p>
                                                    <p>{{ $favorite->title }}</p>
                                                    <p>{{ $favorite->body }}</p>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                {{ $favorites->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        </div>
    </div>
@endsection