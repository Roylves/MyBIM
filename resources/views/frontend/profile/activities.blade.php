@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-3">
            @include('frontend.profile._sidebar')
        </div>

        <div class="col-lg-9">
            <br>
            <div class="row">
                <div class="col-lg-12 col-md-12 mb-12">
                    <div class="card">
                        <div class="card-header">
                            Activities
                        </div>
                        <div class="card-block">
                            <form class="form-inline">
                                <label class="mr-sm-2" for="filter">Filter</label>
                                {{ Form::select('filter', ['all' => 'All', 'downloads' => 'Downloads', 'favorites' => 'Favourites', 'ratings' => 'Ratings', 'others' => 'Others'], request('filter'), ['class' => 'mb-2 mr-sm-2 mb-sm-0']) }}
                                
                                <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                            </form>
                            <br>
                            <div class="table-responsive">
                                <table class="table table small table-striped table-hover">
                                    <tbody>
                                        @foreach ($activities as $activity)
                                            <tr>
                                                <td>
                                                    {{ $activity->created_at->diffForHumans() }}
                                                </td>
                                                <td>
                                                    @if ($activity->content_type == 'File')
                                                        <?php $file = App\Models\File::find($activity->getContentItem()['id']); ?>
                                                        <p>{{ $activity->getDescription() }}</p>
                                                        <p>
                                                            <a href="{{ action('ObjectController@getBySlug', [$file->object->manufacturer->slug, $file->object->slug]) }}">
                                                                <span class="lnr lnr-list"></span>
                                                                {{ $file->object->code }} {{ $file->object->name }}
                                                            </a>
                                                        </p>
                                                        <p>
                                                            <a href="{{ action('ObjectController@download', $file->id) }}">
                                                                <span class="lnr lnr-download"></span>
                                                            </a>
                                                            @foreach ($file->categories as $category)
                                                                <span class="badge badge-info">{{ $category->name }}</span>
                                                            @endforeach
                                                            @include('frontend.home._format_file')
                                                        </p>
                                                    @elseif ($activity->content_type == 'User')
                                                        {{ $activity->getDescription() }}
                                                    @elseif ($activity->content_type == 'Object')
                                                        <?php $object = App\Models\Object::find($activity->getContentItem()['id']); ?>
                                                        <p>{{ $activity->getDescription() }}</p>
                                                        <p>
                                                            <a href="{{ action('ObjectController@getBySlug', [$object->manufacturer->slug, $object->slug]) }}">
                                                                <span class="lnr lnr-list"></span>
                                                                {{ $object->code }} {{ $object->name }}
                                                            </a>
                                                        </p>
                                                    @else
                                                        X {{ $activity->getDescription() }}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                {{ $activities->appends(request()->input())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        </div>
    </div>
@endsection