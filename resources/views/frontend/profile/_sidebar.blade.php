<br>
<div class="card">
    <div class="card-header" role="tab">
        <h6 class="mb-0">
            <a href="{{ action('ProfileController@index') }}">
                Profile
            </a>
        </h6>
    </div>
    <div class="card-header" role="tab">
        <h6 class="mb-0">
            <a href="{{ action('ProfileController@activities') }}">
                Activities
            </a>
        </h6>
    </div>
    <div class="card-header" role="tab">
        <h6 class="mb-0">
            <a href="{{ action('ProfileController@ratings') }}">
                Ratings
            </a>
        </h6>
    </div>
    <div class="card-header" role="tab">
        <h6 class="mb-0">
            <a href="{{ action('ProfileController@favorites') }}">
                Favorites
            </a>
        </h6>
    </div>    
</div>
<br>