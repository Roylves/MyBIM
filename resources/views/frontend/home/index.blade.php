@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-3">
            <br>
            {{ Form::open(['method' => 'GET', 'action' => 'HomeController@index']) }}
                <h6><span class="lnr lnr-funnel"></span> Filters</h6>
                <div class="card">
                    <div class="card-header" role="tab">
                        <h6 class="mb-0">
                            <a class="collapsed" data-toggle="collapse" href="#collapse-manufacturers" aria-expanded="false" aria-controls="collapse-manufacturers">
                                <span class="lnr lnr-circle-minus float-right"></span>
                                Manufacturers
                            </a>
                        </h6>
                    </div>
                    <div id="collapse-manufacturers" class="collapse show" role="tabpanel">
                        <div class="card-block">
                            @foreach ($manufacturers as $manufacturer)
                                <div class="form-check">
                                    <label class="form-check-label">
                                        {{ Form::checkbox("manufacturer_id[]", $manufacturer->id, in_array($manufacturer->id, Request::get('manufacturer_id', [])), ['class' => 'form-check-input']) }}
                                        {{ $manufacturer->name }}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <br>

                @foreach ($objectCategories as $parentCategory)
                    <div class="card">
                        <div class="card-header" role="tab">
                            <h6 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ $parentCategory->slug}}" aria-expanded="false" aria-controls="collapse-{{ $parentCategory->slug}}">
                                    <span class="lnr lnr-circle-minus float-right"></span>
                                    {{ $parentCategory->name }}
                                </a>
                            </h6>
                        </div>
                        <div id="collapse-{{ $parentCategory->slug}}" class="collapse show" role="tabpanel">
                            <div class="card-block">
                                @foreach ($parentCategory->subCategories as $subCategory)
                                    @if ($subCategory->status == 'PUBLISHED')
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                {{ Form::checkbox("object_category_id[]", $subCategory->id, in_array($subCategory->id, Request::get('object_category_id', [])), ['class' => 'form-check-input']) }}
                                                {{ $subCategory->name }}
                                            </label>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <br>
                @endforeach

                @foreach ($fileCategories as $parentCategory)
                    <div class="card">
                        <div class="card-header" role="tab">
                            <h6 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ $parentCategory->slug}}" aria-expanded="false" aria-controls="collapse-{{ $parentCategory->slug}}">
                                    <span class="lnr lnr-circle-minus float-right"></span>
                                    {{ $parentCategory->name }}
                                </a>
                            </h6>
                        </div>
                        <div id="collapse-{{ $parentCategory->slug}}" class="collapse show" role="tabpanel">
                            <div class="card-block">
                                @foreach ($parentCategory->subCategories as $subCategory)
                                    @if ($subCategory->status == 'PUBLISHED')
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                {{ Form::checkbox("file_category_id[]", $subCategory->id, in_array($subCategory->id, Request::get('file_category_id', [])), ['class' => 'form-check-input']) }}
                                                {{ $subCategory->name }}

                                                {{-- @foreach ($subCategory->subCategories as $superSubCategory)
                                                    @if ($superSubCategory->status == 'PUBLISHED')
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                                                {{ Form::checkbox("file_category_id[]", $superSubCategory->id, in_array($superSubCategory->id, Request::get('file_category_id', [])), ['class' => 'form-check-input']) }}
                                                                {{ $superSubCategory->name }}
                                                            </label>
                                                        </div>
                                                    @endif
                                                @endforeach --}}
                                            </label>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <br>
                @endforeach

                <a class="btn btn-default" href="{{ action('HomeController@index') }}">Clear</a>
                {{ Form::submit('Filter', ['class' => 'btn btn-primary']) }}
            {{ Form::close() }}
            <br>
        </div>

        <div class="col-lg-9">
            <br>
            @if ($objects->count())
                <h6 class="text-right"><small class="text-muted"><span class="lnr lnr-list"> </span>Displaying {{ $objects->count() }} of {{ $objects->total() }} objects</small></h6>
                
                <div class="row">
                    @foreach ($objects as $object)
                        <div class="col-lg-4 col-md-6 mb-4">
                            <div class="card h-100">
                                <a href="{{ action('HomeController@object', [$object->manufacturer->slug, $object->slug]) }}">
                                    @if ($object->image->count())
                                        <img class="card-img-top img-fluid" src="{{ url('storage/' . $object->image->first()->path) }}" alt="">
                                    @else
                                        <img class="card-img-top img-fluid" src='data:image/svg+xml;charset=UTF-8,<svg%20width%3D"500"%20height%3D"500"%20xmlns%3D"http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg"%20viewBox%3D"0%200%2075%2075"%20preserveAspectRatio%3D"none"><defs><style%20type%3D"text%2Fcss">%23holder_15b160a505f%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20<%2Fstyle><%2Fdefs><g%20id%3D"holder_15b160a505f"><rect%20width%3D"75"%20height%3D"75"%20fill%3D"%23777"><%2Frect><g><text%20x%3D"19.34375"%20y%3D"42"><%2Ftext><%2Fg><%2Fg><%2Fsvg>' alt="">
                                    @endif
                                </a>
                                <div class="card-block">
                                    <h5 class="card-title"><a href="{{ action('HomeController@object', [$object->manufacturer->slug, $object->slug]) }}">{{ $object->name }}</a></h5>
                                    <p class="card-text small">
                                        {{ strlen($object->description) > 50 ? substr($object->description, 0, 50) . ' ...' : $object->description }}
                                    </p
                                    <hr>
                                    <div class="table-responsive">
                                        <table class="table table-sm table-striped table-hover">
                                            <tbody>
                                                <tr>
                                                    <td class="small text-muted text-xs-right">Manufacturer</td>
                                                    <td><img height="19" src="{{ url('storage/' . $object->manufacturer->image) }}" alt="{{ $object->manufacturer->name }}"></td>
                                                </tr>
                                                <tr>
                                                    <td class="small text-muted text-xs-right">Code</td>
                                                    <td>{{ $object->code }}</td>
                                                </tr>
                                                @if ($object->properties)
                                                    @foreach ($object->properties as $property => $value)
                                                        @if (in_array($property, ['Length', 'Width', 'Depth', 'Moment', 'Shear', 'length', 'width', 'depth', 'moment', 'shear']))
                                                            <tr>
                                                                <td class="small text-muted text-xs-right">{{ ucfirst($property) }}</td>
                                                                <td>{{ strtoupper($value) }} {{ get_unit($property)}}</td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                @include('frontend.home._category_object')
                                @include('frontend.home._category_file')
                            </div>
                        </div>
                    @endforeach
                </div>
                {{ $objects->appends(['manufacturer_id' => Request::get('manufacturer_id', []), 'object_category_id' => Request::get('object_category_id', [])])->links() }}
            @else
                <br>
                <div class="col-lg-12 col-md-12 mb-12 text-center">
                    <p><span class="lnr lnr-warning"></span> Sorry, no object matched the query.</p>
                </div>      
            @endif
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $(function ($) {
                $('.collapse').on('shown.bs.collapse hidden.bs.collapse', function () {
                    $(this).prev().find('.lnr').toggleClass('lnr-circle-minus lnr-plus-circle');
                })
            });
        });
    </script>
@stop