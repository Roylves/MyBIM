@extends('master')

@section('content')

    <div class="jumbotron" style="background-image: url('assets/images/plugin-header.png');background-size: 1200px 300px; background-repeat: no-repeat;">
        <h1>BIM PLUG-IN FOR AUTODESK REVIT</h1>
        <p>
            @if ($object && $featured)
                <a class="btn btn-lg btn-success" href="{{ action('ObjectController@download', $featured->id) }}" role="button">Download Plugin @if ($featured->categories->count())<span class="small">({{ $featured->categories->first()->name }})</span>@endif</a>
            @else
                <a class="btn btn-lg btn-success" href="#" role="button">Coming Soon</span></a>
            @endif
        </p>
    </div>

    <div class="container marketing">
        <hr class="featurette-divider">
        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">A SINGLE PLUG-IN FOR CIDB AND JKR BIM OBJECTS</span></h2>
                <p class="lead">You can easily search and browse the CIDB and JKR objects in a single plug-in. BIM objects' properties can be used during browsing and searching. So, no more flipping every single catalogue pages to find the BIM objects you want.</p>
            </div>
            <div class="col-md-5">
                <img class="featurette-image img-fluid mx-auto" src="{{ asset('assets/images/img_strip_1_2.png') }}" data-holder-rendered="true">
            </div>
        </div>

        <hr class="featurette-divider">
        <div class="row featurette">
            <div class="col-md-7 push-md-5">
                <h2 class="featurette-heading">A GALLERY-LIKE BIM OBJECTS DISPLAY</span></h2>
                <p class="lead">An attractive and engaging gallery-like BIM objects display allow you to have pleasant view of the BIM objects and their properties during browsing and searching activities.</p>
            </div>
            <div class="col-md-5 pull-md-7">
                <img class="featurette-image img-fluid mx-auto" src="{{ asset('assets/images/img_strip_2_2.png') }}" data-holder-rendered="true">
            </div>
        </div>

        <hr class="featurette-divider">
        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">DRAG-AND-DROP BIM OBJECT</span></h2>
                <p class="lead">There are wide range of BIM objects based on CIDB and JKR catalogues from various manufacturers. You can drag-and-drop these objects directly into your design model. All of the BIM objects have the exact properties as provided by CIDB and JKR.</p>
            </div>
            <div class="col-md-5">
                <img class="featurette-image img-fluid mx-auto" src="{{ asset('assets/images/img_strip_3_2.png') }}" data-holder-rendered="true">
            </div>
        </div>

        @if ($object && $object->files->count())
            <hr class="featurette-divider">
            <div class="row featurette">
                <h3>All Versions</h3>
                <div class="table-responsive">
                    <table class="table table table-striped table-hover">
                        <tbody>
                            @foreach ($object->files as $file)
                                <tr>
                                    <td class="text-center">
                                        @foreach ($file->categories as $category)
                                            <span class="badge badge-pill badge-default">{{ $category->name }}</span>
                                        @endforeach
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ action('ObjectController@download', $file->id) }}"><span class="lnr lnr-download"></span></a>
                                    </td>
                                    <td class="text-center">
                                        {{ $file->download_count }} download(s)
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
        <br>
    </div>
@stop