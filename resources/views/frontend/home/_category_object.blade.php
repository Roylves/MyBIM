<div class="card-footer">
    @if ($object->categories)
        @foreach ($object->categories as $category)
            @if ($category->parent && $category->parent->status == App\Models\Category::STATUS_PUBLISHED)
                @if ($category->status == App\Models\Category::STATUS_PUBLISHED)
                    <span class="badge badge badge-default">{{ $category->name }}</span>
                @endif
            @endif
        @endforeach
    @endif
</div>