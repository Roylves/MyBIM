@if (strpos($file->path, 'ifc'))
    <span class="badge badge-pill badge-danger">IFC</span>
@elseif (strpos($file->path, 'rfa'))
    <span class="badge badge-pill badge-primary">RFA</span>
@elseif (strpos($file->path, 'dwg'))
    <span class="badge badge-pill badge-warning">DWG</span>
@elseif (strpos($file->path, 'rvt'))
    <span class="badge badge-pill badge-default">RVT</span>
@elseif (strpos($file->path, 'zip'))
    <span class="badge badge-pill badge-success">ZIP</span>
@endif