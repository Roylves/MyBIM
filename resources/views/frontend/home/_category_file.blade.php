<div class="card-footer">
    @if ($object->featured) <span class="badge badge-pill badge-success">&#9733;</span> @endif
    @foreach ($object->platforms() as $platform)
        <span class="badge badge-info">{{ $platform->name }}</span>
    @endforeach
</div>