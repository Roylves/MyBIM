@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <br>
            <h1 class="text-center">{{ $manufacturer->name }}</h1>
            <div class="text-center">
                <img height="200" src="{{ url('storage/' . $manufacturer->image) }}" alt="{{ $manufacturer->name }}">
            </div>
            <br>
        </div>
    </div>
@stop