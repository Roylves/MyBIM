<footer>
    <div id="socket-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-copyright">
                    <p>&copy; 2017 myBIM Malaysia. All rights reserved.</p>
                </div>
                <div class="col-sm-6 col-links links-social">
                    <ul class="list-inline">
                        <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="https://www.facebook.com/CIDBOfficial" target="_blank"><span class="sr-only screen-reader-text">Facebook</span></a></li>
                        <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="https://twitter.com/BimCidb" target="_blank"><span class="sr-only screen-reader-text">Twitter</span></a></li>
                        <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="https://www.youtube.com/channel/UCghReyLqnOiEDrLOKQDqDpg" target="_blank"><span class="sr-only screen-reader-text">Youtube</span></a></li>
                    </ul>
                </div>         
            </div>
        </div>
    </div><!--/#socket-footer-->
</footer>

<div id="auth-modal" class="modal fade" tabindex="-1" role="basic" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div>

<div id="forgot-modal" class="modal fade" tabindex="-1" role="basic" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div>

<script type='text/javascript' src="{{ asset('assets/v2/js/bootstrap.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('assets/v2/js/modernizr-custom.js') }}"></script>
<script type='text/javascript' src="{{ asset('assets/v2/js/plugins.min.js') }}"></script>

<!-- modal form scripts -->
<script src="{{ asset('assets/v2/js/jquery.validate.js') }}"></script>
<script src="{{ asset('assets/v2/js/custom/validation-settings.js') }}"></script>
<script src="{{ asset('assets/v2/js/jquery.form.js') }}"></script>
<script src="{{ asset('assets/v2/js/custom/form-submission.js') }}"></script>
<script>
    $(function($) {
        $('body').on('show.bs.modal', '#auth-modal', function () {
            $(this).find('.modal-content').html('');
        });
        $('body').on('shown.bs.modal', '#auth-modal', function (e) {
            $(this).find(".modal-content").load($(e.relatedTarget).data('href'), function() {
                $('#auth-modal form').validate($.extend({}, validationObject));
                ajaxForm($('#auth-modal form'));
            });
        });

        $('body').on('show.bs.modal', '#forgot-modal', function () {
            $('#auth-modal').modal('hide');
            $(this).find('.modal-content').html('');
        });
        $('body').on('shown.bs.modal', '#forgot-modal', function (e) {
            $(this).find(".modal-content").load($(e.relatedTarget).data('href'), function() {
                $('#forgot-modal form').validate($.extend({}, validationObject));
                ajaxForm($('#forgot-modal form'));
            });
        });
    });
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-98448001-1', 'auto');
  ga('send', 'pageview');

</script>

@yield('javascript')