<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Login Now</h4>
</div>
<div class="modal-body">
    <form method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="alert alert-danger hidden" style="border: none;"></div>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}"">
            <label for="email">Email Address</label>
            <input type="email" id="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}"">
            <label for="password">Password</label>
            <input type="password" id="password" class="form-control" name="password" placeholder="Password" required>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> <small>Remember Me</small>
            </label>
        </div>
        <button type="submit" class="btn btn-secondary btn-block">Submit</button>
        <!--div class="text-center"><small><a data-href="{{ action('Auth\ForgotPasswordController@showLinkRequestForm') }}" data-toggle="modal" data-target="#forgot-modal">Forgot your password?</a></small></div-->
    </form>
    <hr />
    <div class="text-center margin-bottom"><small>OR</small></div>
    <div class="clearfix">
        <a class="btn btn-facebook btn-block" href="{{ url('login/facebook') }}"><i class="fa fa-facebook pull-left"></i> <small>Login with Facebook</small></a>
        <a class="btn btn-google-plus btn-block" href="{{ url('login/google') }}"><i class="fa fa-google-plus pull-left"></i> <small>Login with Google Plus</small></a>
    </div>
</div>