<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Reset Password</h4>
</div>
<div class="modal-body">
    <form method="POST" action="{{ route('password.email') }}">
        {{ csrf_field() }}
        <div class="alert alert-danger hidden" style="border: none;"></div>
        <div class="form-group">
            <label for="email">Email Address</label>
            <input type="email" id="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
        </div>
        <button type="submit" class="btn btn-secondary btn-block">Reset Password</button>
    </form>
</div>