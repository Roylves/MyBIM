<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Reset Password</h4>
</div>
<div class="modal-body">
    <form method="POST" action="{{ route('password.request') }}">
        {{ csrf_field() }}
        <input type="hidden" name="token" value="{{ $token }}">
        <div class="alert alert-danger hidden" style="border: none;"></div>
        <div class="form-group">
            <label for="email">Email Address</label>
            <input type="email" id="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" id="password" class="form-control" name="password" placeholder="Password" required autofocus>
        </div>
        <div class="form-group">
            <label for="password_confirmation">Confirm Password</label>
            <input type="password" id="password_confirmation" class="form-control" name="password_confirmation" laceholder="Confrim Password" required autofocus>
        </div>
        <button type="submit" class="btn btn-secondary btn-block">Reset Password</button>
    </form>
</div>