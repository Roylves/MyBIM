<!DOCTYPE html>
<html lang="en-US" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <!-- Bootplate v1.9 -->
        
    <meta name="description" content="Malaysia BIM Objects Library" />
    <meta name="author" content="myBIM Malaysia. All rights reserved" />
    <meta itemprop="name" content="{{ url('') }}" />
    <meta itemprop="description" content="Malaysia BIM Objects Library" />
    <meta property="og:site_name" content="myBIM Library" />
    @stack('meta')
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="myBIM Library">
    <meta name="robots" content="noodp"/>
    
    <title>Find BIM Objects &#8211; myBIM Library</title>
    <meta name='robots' content='noindex,follow' />
    <link rel='dns-prefetch' href='//ajax.googleapis.com' />
    <link rel='dns-prefetch' href='//cdnjs.cloudflare.com' />
    <link rel='dns-prefetch' href='//oss.maxcdn.com' />
    <link rel="stylesheet" id="font-awesome-css"  href="{{ asset('assets/v2/css/font-awesome.min.css') }}" type="text/css" media="all" />
    <link rel="stylesheet" id="bootstrap-css"  href="{{ asset('assets/v2/css/bootstrap.min.css') }}"" type='text/css' media='all' />
    <link rel="stylesheet" id="bootplate-css"  href="{{ asset('assets/v2/css/parent.min.css') }}" type='text/css' media='all' />
    <link rel="stylesheet" id="mybim-css"  href="{{ asset('assets/v2/css/style.min.css') }}" type='text/css' media='all' />
    <!--[if lt IE 9]>
    <link rel='stylesheet' id='bootplate-ie-css'  href='{{ asset('assets/v2/css/ie.css') }}' type='text/css' media='all' />
    <![endif]-->
    @stack('style')

    <script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js'></script>
    <!--[if lt IE 9]>
    <script type='text/javascript' src='//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js'></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script type='text/javascript' src='//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js'></script>
    <![endif]-->
    
    <link rel="preload" href="{{ asset('assets/v2/css/body.min.css') }}" as="style" onload="this.rel='stylesheet'" type="text/css" />
    <script src="{{ asset('assets/v2/js/loadcss.min.js') }}" type="text/javascript"></script>
</head>

<body>
    <nav class="navbar navbar-full navbar-fixed-top navbar-default navbar-img-logo navbar-light-logo hamburger-cross">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggler hidden-sm-up is-closed navbar-toggle collapsed" data-toggle="collapse" data-target="#primary-menu" aria-expanded="false" aria-controls="primary-menu">
                    <span class="sr-only screen-reader-text">Toggle Menu</span>
                    <span class="hamb-top"></span>
                    <span class="hamb-middle"></span>
                    <span class="hamb-bottom"></span>
                </button>
                <a href="{{ env('PORTAL_URL') }}" class="navbar-brand" rel="home"><img src="{{ asset('assets/v2/images/logo-mybim.png') }}" alt="myBIM Library" /></a>
            </div>
            <div id="primary-menu" class="collapse navbar-collapse pull-sm-right navbar-toggleable-xs">
                <ul class="nav navbar-nav">
                    <li class="menu-item menu-item-type-post_type menu-item-object-page"><a title="Home" href="{{ env('PORTAL_URL') }}">Home</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page {{ (Route::getCurrentRoute()->getActionName() == 'App\Http\Controllers\HomeController@index' || Route::getCurrentRoute()->getActionName() == 'App\Http\Controllers\ObjectController@getBySlug') ? 'active' : '' }}"><a title="Objects" href="{{ url('') }}">Objects</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page"><a title="Plug-in" href="{{ env('PORTAL_URL') }}/plug-in">Plug-in</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page"><a title="Guidelines" href="{{ env('PORTAL_URL') }}/guidelines">Guidelines</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page"><a title="Knowledge" href="{{ env('PORTAL_URL') }}/knowledge">Knowledge</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page"><a title="News" href="{{ env('PORTAL_URL') }}/news">News</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page"><a title="FAQ" href="{{ env('PORTAL_URL') }}/faq">FAQ</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page"><a title="Contact" href="{{ env('PORTAL_URL') }}/contact">Contact</a></li>
                </ul>
                
                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li>
                            <a style="cursor: pointer;" title="Register" data-href="{{ action('Auth\RegisterController@showRegistrationForm') }}" data-toggle="modal" data-target="#auth-modal">Register</a>
                        </li>
                        <li>
                            <a style="cursor: pointer;" title="Login" data-href="{{ action('Auth\LoginController@showLoginForm') }}" data-toggle="modal" data-target="#auth-modal">Login</a>
                        </li>
                    @else
                        <li>
                            <a href="{{ action('ProfileController@index') }}">{{ Auth::user()->name }}</a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    @endif
                </ul>
            </div><!-- /#primary-menu -->
        </div><!--/.container-->
    </nav>
    <a class="skip-link sr-only screen-reader-text" href="#content">Skip to content</a>

    @if (Route::getCurrentRoute()->getActionName() == 'App\Http\Controllers\HomeController@index')
        <header class="jumbotron jumbotron-fluid bg-custom has-featured-image" style="background-image:url({{ asset('assets/v2/images/bg-home.png') }});">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h1>Find BIM Objects</h1>           
                        <p>Browse our vast library containing thousands of generic and manufacturer BIM objects.</p>                      
                    </div>
                </div>
            </div><!--/.container-->
        </header>
    @endif

    @yield('content')

    @include('frontend.v2.footer')

    @stack('scripts')
</body>
</html>