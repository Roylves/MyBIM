@if (strpos($file->path, 'ifc'))
    IFC
@elseif (strpos($file->path, 'rfa'))
    RFA
@elseif (strpos($file->path, 'dwg'))
    DWG
@elseif (strpos($file->path, 'rvt'))
    RVT
@elseif (strpos($file->path, 'zip'))
    ZIP
@endif