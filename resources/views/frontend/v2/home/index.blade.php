@extends('frontend.v2.master')

@section('content')
    <section class="bg-secondary objects objects-main">    
        <div class="container">
            <div class="row padding-70">
                @if ($objects->count())
                    <div class="col-md-12 col-sm-12 ">
                        <h6 class="text-right">
                            <span class="fa fa-list"></span> Displaying {{ ($objects->currentpage() - 1 ) * $objects->perpage() + 1 }} to {{ $objects->total() <= $objects->currentpage() * $objects->perpage() ? $objects->total() : $objects->currentpage() * $objects->perpage() }} of {{ $objects->total() }} objects
                        </h6>
                    </div>
                @endif
                <div class="col-md-3">
                    <div class="panel panel-default panel-filters">
                        <div class="panel-heading clearfix">
                            <span class="panel-title hidden-xs hidden-sm">Search Filters</span>
                            <a data-toggle="collapse" href="#filter" class="visible-xs visible-sm pull-left"><i class="fa fa-chevron-down pull-left" aria-hidden="true"></i> Search Filters</a>

                            <div class="pull-right">
                                <a href="{{ action('HomeController@index') }}">
                                    <small>Reset <i class="fa fa-refresh" aria-hidden="true"></i></small>
                                </a>
                            </div>
                        </div>
                        <div id="filter" class="panel-body collapse">
                            <div class="row">
                                {{ Form::open(['method' => 'GET', 'action' => 'HomeController@index']) }}
                                    <div class="col-md-12">
                                        <h3>Keyword</h3>
                                        {{ Form::text("keyword", Request::get('keyword'), ['class' => 'form-control', 'placeholder' => 'e.g. Slab']) }}
                                        <hr />
                                    </div>

                                    @foreach ($objectCategories as $parentCategory)
                                        <div class="col-md-12">
                                            <h3>{{ $parentCategory->name }}</h3>
                                                @foreach ($parentCategory->subCategories as $subCategory)
                                                    @if ($subCategory->status == 'PUBLISHED')
                                                        <div class="checkbox">
                                                            <label>
                                                                {{ Form::checkbox("object_category_id[]", $subCategory->id, in_array($subCategory->id, Request::get('object_category_id', []))) }} {{ $subCategory->name }}
                                                            </label>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            <hr />
                                        </div>
                                    @endforeach

                                    @if (count($fileCategories))
                                        @foreach ($fileCategories as $parentCategory)
                                            <div class="col-md-12">
                                                <h3>{{ $parentCategory->name }}</h3>
                                                    @foreach ($parentCategory->subCategories as $subCategory)
                                                        @if ($subCategory->status == 'PUBLISHED')
                                                            <div class="checkbox">
                                                                <label>
                                                                    {{ Form::checkbox("object_category_id[]", $subCategory->id, in_array($subCategory->id, Request::get('object_category_id', []))) }} {{ $subCategory->name }}
                                                                </label>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                <hr />
                                            </div>
                                        @endforeach
                                    @endif

                                    <div class="col-md-12">
                                        {{ Form::submit('Filter', ['class' => 'btn btn-secondary']) }}
                                    </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-9">
                    <div class="row">
                        @if ($objects->count())
                            @foreach ($objects as $object)
                                <div class="col-md-4 col-sm-6">
                                    <div class="panel panel-default panel-objects">
                                        <div class="panel-body">
                                            <div class="add-to-fav" data-id="{{ $object->id }}" @if (!Auth::check()) data-toggle="tooltip" data-placement="left" title="Please login to favourite" @endif>
                                                <span class="sr-only screen-reader-text">Add to Favourite</span><i class="fa fa-thumbs-up {{ $object->isFavorited() ? 'fa-active' : '' }}" aria-hidden="true"></i></span>
                                            </div>
                                            <a href="{{ action('ObjectController@getBySlug', [$object->manufacturer->slug, $object->slug]) }}">
                                            @if ($object->image->count())
                                                <img class="img-responsive" src="{{ url('storage/' . $object->image->first()->path) }}" alt="">
                                            @else
                                                <img class="img-responsive" src='data:image/svg+xml;charset=UTF-8,<svg%20width%3D"500"%20height%3D"500"%20xmlns%3D"http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg"%20viewBox%3D"0%200%2075%2075"%20preserveAspectRatio%3D"none"><defs><style%20type%3D"text%2Fcss">%23holder_15b160a505f%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20<%2Fstyle><%2Fdefs><g%20id%3D"holder_15b160a505f"><rect%20width%3D"75"%20height%3D"75"%20fill%3D"%23777"><%2Frect><g><text%20x%3D"19.34375"%20y%3D"42"><%2Ftext><%2Fg><%2Fg><%2Fsvg>' alt="">
                                            @endif
                                            <small>{{ $object->code }}</small>
                                            <h3>{{ $object->name }}</h3>
                                            </a>
                                            <table class="table table-striped">
                                                {{-- @if ($object->categories)
                                                    @foreach ($object->categories as $category)
                                                        @if ($category->parent && $category->parent->status == App\Models\Category::STATUS_PUBLISHED)
                                                            @if ($category->status == App\Models\Category::STATUS_PUBLISHED)
                                                                <tr>
                                                                    <td>{{ $category->parent->name }}</td>
                                                                    <td><a href="{{ action('HomeController@index', ['object_category_id[]' => $category->id]) }}">{{ $category->name }}</a></td>
                                                                </tr>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @endif --}}
                                                @if ($object->properties)
                                                    @foreach ($object->properties as $property => $value)
                                                        @if (in_array($property, ['Length', 'Width', 'Height', 'length', 'width', 'height']))
                                                            <tr>
                                                                <td>{{ ucfirst($property) }}</td>
                                                                <td>{{ strtoupper($value) }} {{ get_unit($property)}}</td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <tr>
                                                    <td>Downloads</td>
                                                    <td>{{ number_format($object->download_count()) }}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        @else
                            <div class="col-lg-12 col-md-12 text-center">
                                <p><span class="fa fa-warning"></span> Sorry, no object matched the query.</p>
                            </div>      
                        @endif

                        <div class="col-lg-12 col-md-12 text-center">
                            {{ $objects->appends(['keyword' => Request::get('keyword'), 'manufacturer_id' => Request::get('manufacturer_id', []), 'object_category_id' => Request::get('object_category_id', [])])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@push('scripts')
    <script>
        @if (!Auth::check())
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        @else
            $('.add-to-fav').click(function(event) {
                var objectId = $(this).data('id');
                $.post("{{ action('ObjectController@index') }}" + '/' + objectId + '/favorite', function( data ) {
                });
                $(event.target).closest('.fa-thumbs-up').toggleClass('fa-active');
            });
        @endif
    </script>
@endpush