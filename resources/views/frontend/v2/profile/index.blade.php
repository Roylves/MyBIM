@extends('frontend.v2.master')

@section('content')
    <section class="bg-secondary">    
        <div class="container">
            <div class="row padding-70">
                <div class="col-md-3">
                    @include('frontend.v2.profile._sidebar')
                </div>

                <div class="col-md-9">
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Profile</div>
                                <form action="{{ action('ProfileController@updateProfile') }}" method="POST"> 
                                    {{ method_field("PUT") }}
                                    {{ csrf_field() }}

                                    <div class="panel-body">
                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif

                                        @if (session('status'))
                                            <div class="alert alert-success">
                                                {{ session('status') }}
                                            </div>
                                        @endif

                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            {{ Form::text('name', Auth::user()->name, ['id' => 'name', 'class' => 'form-control', 'required']) }}
                                        </div>

                                        <div class="form-group">
                                            <label for="name">Email</label>
                                            {{ Form::text('email', Auth::user()->email, ['id' => 'email', 'class' => 'form-control']) }}
                                        </div>

                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <br>
                                            <small>Leave empty to keep the same</small>
                                            {{ Form::password('password', ['id' => 'password', 'class' => 'form-control', 'placeholder' => 'Password']) }}
                                        </div>

                                        <div class="form-group">
                                            <label for="profession">Profession</label>
                                            {{ Form::select('profession_id', ['' => 'Select Profession'] + App\Models\Profession::pluck('name', 'id')->toArray(), Auth::user()->profession_id, ['id' => 'profession', 'class' => 'form-control', 'required']) }}
                                        </div>
                                    </div>

                                    <div class="panel-footer">
                                        <button type="submit" class="btn btn-secondary">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop