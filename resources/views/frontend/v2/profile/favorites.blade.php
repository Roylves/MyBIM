@extends('frontend.v2.master')

@section('content')
    <section class="bg-secondary">    
        <div class="container">
            <div class="row padding-70">
                <div class="col-md-3">
                    @include('frontend.v2.profile._sidebar')
                </div>

                <div class="col-md-9">
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Favorites</div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table small table-striped table-hover">
                                            <tbody>
                                                @foreach ($favorites as $favorite)
                                                    <tr>
                                                        <td>
                                                            {{ $favorite->created_at->diffForHumans() }}
                                                        </td>
                                                        <td>
                                                            <p>
                                                                <a href="{{ action('ObjectController@getBySlug', [$favorite->favoriteable->manufacturer->slug, $favorite->favoriteable->slug]) }}" target="_blank">
                                                                    {{ $favorite->favoriteable->code }} {{ $favorite->favoriteable->name }}
                                                                </a>
                                                            </p>
                                                            <p>
                                                                @for ($i = 0; $i < $favorite->rating; $i++)
                                                                    &#9734;
                                                                @endfor
                                                            </p>
                                                            <p>{{ $favorite->title }}</p>
                                                            <p>{{ $favorite->body }}</p>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>

                                        {{ $favorites->links() }}
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop