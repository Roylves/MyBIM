@extends('frontend.v2.master')

@section('content')
    <section class="bg-secondary">    
        <div class="container">
            <div class="row padding-70">
                <div class="col-md-3">
                    @include('frontend.v2.profile._sidebar')
                </div>

                <div class="col-md-9">
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Activities</div>
                                <div class="panel-body">
                                    <form class="form-inline">
                                        <label for="filter">Filter</label>
                                        {{ Form::select('filter', ['all' => 'All', 'downloads' => 'Downloads', 'favorites' => 'Favourites', 'ratings' => 'Ratings', 'others' => 'Others'], request('filter'), ['class' => 'mb-2 mr-sm-2 mb-sm-0']) }}
                                        
                                        <button type="submit" class="btn btn-secondary">Submit</button>
                                    </form>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table small table-striped table-hover">
                                            <tbody>
                                                @foreach ($activities as $activity)
                                                    <tr>
                                                        <td>
                                                            {{ $activity->created_at->diffForHumans() }}
                                                        </td>
                                                        <td>
                                                            @if ($activity->content_type == 'File')
                                                                <?php $file = App\Models\File::find($activity->getContentItem()['id']); ?>
                                                                <p>{{ $activity->getDescription() }}</p>
                                                                <p>
                                                                    @if ($file->isPlugin() === true)
                                                                        <a href="{{ env('PLUGIN_URL') }}" target="_blank">
                                                                            <span class="fa fa-list"></span>
                                                                            {{ $file->object->code }} {{ $file->object->name }}
                                                                        </a>
                                                                    @else
                                                                        <a href="{{ action('ObjectController@getBySlug', [$file->object->manufacturer->slug, $file->object->slug]) }}" target="_blank">
                                                                            <span class="fa fa-list"></span>
                                                                            {{ $file->object->code }} {{ $file->object->name }}
                                                                        </a>
                                                                    @endif
                                                                </p>
                                                                <p>
                                                                    <a href="{{ action('ObjectController@download', $file->id) }}" target="_blank">
                                                                        <span class="fa fa-download"></span>
                                                                    </a>
                                                                    @foreach ($file->categories as $category)
                                                                        {{ $category->name }}
                                                                    @endforeach
                                                                </p>
                                                            @elseif ($activity->content_type == 'User')
                                                                {{ $activity->getDescription() }}
                                                            @elseif ($activity->content_type == 'Object')
                                                                <?php $object = App\Models\Object::find($activity->getContentItem()['id']); ?>
                                                                <p>{{ $activity->getDescription() }}</p>
                                                                <p>
                                                                    <a href="{{ action('ObjectController@getBySlug', [$object->manufacturer->slug, $object->slug]) }}" target="_blank">
                                                                        <span class="fa fa-list"></span>
                                                                        {{ $object->code }} {{ $object->name }}
                                                                    </a>
                                                                </p>
                                                            @else
                                                                X {{ $activity->getDescription() }}
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>

                                        {{ $activities->appends(request()->input())->links() }}
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop