<br>
<div class="row">
    <div class="col-md-12">
        <div class="list-group">
            <a class="list-group-item" href="{{ action('ProfileController@index') }}">Profile</a>
            <a class="list-group-item" href="{{ action('ProfileController@activities') }}">Activities</a>
            <a class="list-group-item" href="{{ action('ProfileController@ratings') }}">Ratings</a>
            <a class="list-group-item" href="{{ action('ProfileController@favorites') }}">Favorites</a>
        </div>
    </div>
</div>