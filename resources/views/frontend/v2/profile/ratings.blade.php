@extends('frontend.v2.master')

@section('content')
    <section class="bg-secondary">    
        <div class="container">
            <div class="row padding-70">
                <div class="col-md-3">
                    @include('frontend.v2.profile._sidebar')
                </div>

                <div class="col-md-9">
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Ratings</div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table small table-striped table-hover">
                                            <tbody>
                                                @foreach ($ratings as $review)
                                                    <tr>
                                                        <td>
                                                            {{ $review->created_at->diffForHumans() }}
                                                        </td>
                                                        <td>
                                                            <p>
                                                                <a href="{{ action('ObjectController@getBySlug', [$review->reviewrateable->manufacturer->slug, $review->reviewrateable->slug]) }}" target="_blank">
                                                                    {{ $review->reviewrateable->code }} {{ $review->reviewrateable->name }}
                                                                </a>
                                                            </p>
                                                            <p>
                                                                @for ($i = 0; $i < $review->rating; $i++)
                                                                    &#9734;
                                                                @endfor
                                                            </p>
                                                            <p>{{ $review->title }}</p>
                                                            <p>{{ $review->body }}</p>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>

                                        {{ $ratings->links() }}
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop