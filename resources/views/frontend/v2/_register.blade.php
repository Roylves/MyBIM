<div class="modal fade" id="mybimregister" tabindex="-1" role="dialog" aria-labelledby="mybimregisterLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Register Now</h4>
            </div>
            <div class="modal-body">
                <div class="text-center">Register with <a href="{{ url('login/facebook') }}">Facebook</a> or <a href="{{ url('login/google') }}">Google Plus</a></div>
                <hr />
                <form method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">Name</label>
                        <input type="text" name="name" value="{{ old('name') }}" class="form-control" id="name" placeholder="Jane Doe" required autofocus>
                        @if ($errors->has('name'))
                            <span class="small alert-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">Email Address</label>
                        <input type="email" name="email" value="{{ old('email') }}" class="form-control" id="email" placeholder="Email" required autofocus>
                        @if ($errors->has('email'))
                            <span class="small alert-danger">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
                        @if ($errors->has('password'))
                            <span class="small alert-danger">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation">Confirm Password</label>
                        <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Password" required>
                    </div>
                    <div class="form-group{{ $errors->has('profession_id') ? ' has-error' : '' }}">
                        <label for="profession_id">Profession</label>
                        {{ Form::select('profession_id', ['' => 'Select Profession'] + App\Models\Profession::where('status', 'PUBLISHED')->pluck('name', 'id')->toArray(), null, ['id' => 'profession_id', 'class' => 'form-control', 'required']) }}
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="accept" required> <small>By clicking Register, you agree to our <a href="#">Terms & Conditions</a></small>
                        </label>
                    </div>
                    <button type="submit" class="btn btn-secondary btn-block">Register</button>
                </form>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script type="text/javascript">
        @if ($errors->count())
            // $('#mybimregister').modal('show');
        @endif
    </script>
@endpush