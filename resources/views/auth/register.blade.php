@extends('master')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/css/auth.css') }}">
@stop

@section('content')
    <div class="col-md-12">
        <div class="nb-login">
            <h3 class="center">Register</h3>
            <form method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}
                
                <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                    <input type="name" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name" required autofocus>
                    @if ($errors->has('name'))
                        <div class="form-control-feedback small">{{ $errors->first('name') }}</div>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                    @if ($errors->has('email'))
                        <div class="form-control-feedback small">{{ $errors->first('email') }}</div>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                    <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                    @if ($errors->has('password'))
                        <div class="form-control-feedback small">{{ $errors->first('password') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                </div>

                <div class="form-group{{ $errors->has('profession_id') ? ' has-danger' : '' }}">
                    {{ Form::select('profession_id', ['' => 'Select Profession'] + App\Models\Profession::where('status', 'PUBLISHED')->pluck('name', 'id')->toArray(), null, ['class' => 'form-control', 'required']) }}
                    @if ($errors->has('profession_id'))
                        <div class="form-control-feedback small">{{ $errors->first('profession_id') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="accept">
                        <input id="accept" type="checkbox" name="accept" required> By clicking Register, you confirm that you have read the <a href="#" target="_blank">terms and conditions</a>, that you
understand them and that you agree to be bound by them.
                    </label>
                </div>

                <button type="submit" class="btn btn-block">Register</button>
            </form>
        </div>
    </div>
@stop