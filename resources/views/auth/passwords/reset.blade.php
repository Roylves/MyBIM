@extends('master')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/css/auth.css') }}">
@stop

@section('content')
    <div class="col-md-12">
        <div class="nb-login">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <h3 class="center">Reset Password</h3>
            <form method="POST" action="{{ route('password.request') }}">
                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                    @if ($errors->has('email'))
                        <div class="form-control-feedback small">{{ $errors->first('email') }}</div>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                    <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                    @if ($errors->has('password'))
                        <div class="form-control-feedback small">{{ $errors->first('password') }}</div>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-danger' : '' }}">
                    <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                    @if ($errors->has('passwopassword_confirmationrd'))
                        <div class="form-control-feedback small">{{ $errors->first('password_confirmation') }}</div>
                    @endif
                </div>
                <button type="submit" class="btn btn-block">Reset Password</button>
            </form>
        </div>
    </div>
@endsection
