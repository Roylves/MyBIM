@extends('master')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/css/auth.css') }}">
@stop

@section('content')
    <div class="col-md-12">
        <div class="nb-login">
            <h3 class="center">Sign In USING EMAIL</h3>
            <form method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                    @if ($errors->has('email'))
                        <div class="form-control-feedback small">{{ $errors->first('email') }}</div>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                    <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                    @if ($errors->has('password'))
                        <div class="form-control-feedback small">{{ $errors->first('password') }}</div>
                    @endif
                </div>

                <div class="checkbox small">
                    <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                    </label>
                </div>
                <button type="submit" class="btn btn-block">Login</button>
                <div class="center">
                    <a class="small" href="{{ route('password.request') }}">Forgot Your Password?</a>
                </div>
            </form>
            <div class="center or">OR</div>
            <h3 class="center">Sign In Using</h3>
            <div class="social">
                <a href="{{ url('login/facebook') }}" class="facebook">&nbsp; Login with Facebook</a>
                <a href="{{ url('login/google') }}" class="google-plus">&nbsp; Login with Google</a>
            </div>
        </div>
    </div>
@stop