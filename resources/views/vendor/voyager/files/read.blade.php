@extends('voyager::master')

@section('page_title','View '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> Viewing {{ ucfirst($dataType->display_name_singular) }} &nbsp;

        @if (Voyager::can('edit_'.$dataType->name))
        <a href="{{ route('voyager.'.$dataType->slug.'.edit', $dataTypeContent->getKey()) }}" class="btn btn-info">
            <span class="glyphicon glyphicon-pencil"></span>&nbsp;
            Edit
        </a>
        @endif
        <a href="{{ route('voyager.'.$dataType->slug.'.index') }}" class="btn btn-warning">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            Return to List
        </a> 
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered" style="padding-bottom:5px;">

                    <!-- /.box-header -->
                    <!-- form start -->

                    <div class="panel-heading" style="border-bottom:0;">
                        <h3 class="panel-title">Object & Manufacturer</h3>
                    </div>
                    <div class="panel-body" style="padding-top:0;">
                        <p>{{ $dataTypeContent->object_id ? $dataTypeContent->object->name : 'No object selected' }}</p>
                        <p>{{ $dataTypeContent->object_id ? $dataTypeContent->object->manufacturer_id ? $dataTypeContent->object->manufacturer->name  : 'No manufacturer selected' : 'No object selected' }}</p>
                    </div>
                    <hr style="margin:0;">

                    <div class="panel-heading" style="border-bottom:0;">
                        <h3 class="panel-title">Path</h3>
                    </div>
                    <div class="panel-body" style="padding-top:0;">
                        <p>{{ url('storage/' .$dataTypeContent->path) }}</p>
                    </div>
                    <hr style="margin:0;">

                    <div class="panel-heading" style="border-bottom:0;">
                        <h3 class="panel-title">Mime</h3>
                    </div>
                    <div class="panel-body" style="padding-top:0;">
                        <p>{{ $dataTypeContent->mime }}</p>
                    </div>
                    <hr style="margin:0;">

                    <div class="panel-heading" style="border-bottom:0;">
                        <h3 class="panel-title">Size</h3>
                    </div>
                    <div class="panel-body" style="padding-top:0;">
                        <p>{{ $dataTypeContent->size }} kB</p>
                    </div>
                    <hr style="margin:0;">

                    <div class="panel-heading" style="border-bottom:0;">
                        <h3 class="panel-title">Type</h3>
                    </div>
                    <div class="panel-body" style="padding-top:0;">
                        <p>{{ $dataTypeContent->type }}</p>
                    </div>
                    <hr style="margin:0;">

                    <div class="panel-heading" style="border-bottom:0;">
                        <h3 class="panel-title">Download Count</h3>
                    </div>
                    <div class="panel-body" style="padding-top:0;">
                        <p>{{ $dataTypeContent->download_count }} download(s)</p>
                    </div>
                    <hr style="margin:0;">

                    <div class="panel-heading" style="border-bottom:0;">
                        <h3 class="panel-title">Categories</h3>
                    </div>
                    <div class="panel-body" style="padding-top:0;">
                        <ul>
                            @if ($dataTypeContent->categories->count())
                                @foreach ($dataTypeContent->categories as $category)
                                    <li>{{ $category->name }}</li>
                                @endforeach
                            @else
                                <li>No categories available</li>
                            @endif
                        </ul>
                    </div>
                    <hr style="margin:0;">

                    <div class="panel-heading" style="border-bottom:0;">
                        <h3 class="panel-title">Created at</h3>
                    </div>
                    <div class="panel-body" style="padding-top:0;">
                        <p>{{ $dataTypeContent->created_at }}</p>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            @if ($dataTypeContent->type == 'IMAGE')
                <div class="col-md-6">
                    <div>
                        <h3 class="panel-title">File</h3>
                        <img style="max-width:640px" src="{!! url('storage/' . $dataTypeContent->path) !!}">
                    </div>
                </div>
            @elseif ($dataTypeContent->type == 'STL')
                <div class="col-md-6">
                    <div id="cjcwrap">
                        <h3 class="panel-title">FILE</h3>
                        <div id="cjcdrag">
                            <span id="titlet1"></span><br>
                            <span id="titlet2" onclick="if (waiting) return false;$id('fileselect').click();"></span>
                            <div style="text-align:center;"></div>
                        </div>
                        <div id="cjcpbar" style="display:none;text-align:center;">
                            <span id="pgt1">Reading file ...</span><br>
                            <progress id="file_pbar"  value="0" max="1"></progress><br><span id="fcancel" onclick="cancel_download=true;">(cancel)</span>
                        </div>
                        <div id="cjcproc" style="display:none;text-align:center;">
                            <span id="prt1">Processing ...</span><br>
                        </div>
                        <div id="cjc" onmousedown="/*$id('rrotate2').checked=true;*/controls.autoRotate=false;" style="display:none;text-align:center;"></div>
                    </div>
                </div>
            @else
                <div class="col-md-6">
                    <div>
                        <h3 class="panel-title">FILE</h3>
                        <div class="text-center">
                            <a target="_blank" href="{!! url('storage/' . $dataTypeContent->path) !!}">
                                {!! url('storage/' . $dataTypeContent->path) !!}
                            </a>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@stop

@section('javascript')
    @if ($isModelTranslatable)
    <script>
        $(document).ready(function () {
            $('.side-body').multilingual();
        });
    </script>
    <script src="{{ config('voyager.assets_path') }}/js/multilingual.js"></script>
    @endif
    @if ($dataTypeContent->type == 'STL')
        <script src="{{ asset('vendor/stl/three.min.js') }}"></script>
        <script src="{{ asset('vendor/stl/webgl_detector.js') }}"></script>
        <script src="{{ asset('vendor/stl/OrbitControls.js') }}"></script>
        <script src="{{ asset('vendor/stl/parser.js') }}"></script>
        <script src="{{ asset('vendor/stl/read_internal.js') }}"></script>
        <script src="{{ asset('vendor/stl/read_external.js') }}"></script>
        <script>
            function $id(e){return document.getElementById(e)}function set_orientation(e){if(orientation=e,mesh){switch(mesh.rotation.x=0,mesh.rotation.y=0,mesh.rotation.z=0,e){case"right":mesh.rotateY(-deg90);break;case"left":mesh.rotateY(deg90);break;case"top":mesh.rotateX(deg90);break;case"bottom":mesh.rotateX(-deg90);break;case"back":mesh.rotateY(2*deg90)}mesh_is_ready&&(mesh.geometry.verticesNeedUpdate=!0)}}function set_edges(e){has_edges=e,has_edges?(egh=new THREE.EdgesHelper(mesh,0),egh.material.linewidth=1,scene.add(egh)):remove_edges()}function remove_edges(){egh&&scene.remove(egh),egh=null}function set_view_units(e){view_units=e,$id("vunits").innerHTML="1"==e?"mm":"in",setCookie("units","1"==e?"mm":"in",1e3),recalc_units()}function set_file_units(e){file_units=e,recalc_units()}function recalc_units(){file_units==view_units?set_vol_and_size(vol,xsize,ysize,zsize):1==file_units?set_vol_and_size(vol/16387.064,xsize/25.4,ysize/25.4,zsize/25.4):set_vol_and_size(16387.064*vol,25.4*xsize,25.4*ysize,25.4*zsize)}function geo_to_vf(e){var t=[],o=[],r=e.vertices.length;for(i=0;i<r;i++)t.push([e.vertices[i].x,e.vertices[i].y,e.vertices[i].z]);var r=e.faces.length;for(i=0;i<r;i++)o.push([e.faces[i].a,e.faces[i].b,e.faces[i].c]);return{vertices:t,faces:o}}function send_to_ts(){if(!waiting&&mesh){cancel_3dp=!1;var e=$id("pbar_3dp");e.value=0,e.style.visibility="visible",$id("pwait_txt").innerHTML="Preparing file for 3D Printing",$id("c3dp").style.visibility="visible",$id("wtext").style.height="80px",$id("pwait").style.display="block";var i=new XMLHttpRequest;i.upload.addEventListener("progress",function(t){return cancel_3dp?void i.abort():(e.value=t.loaded/t.total,void(e.value>.99&&($id("pwait_txt").innerHTML="Sending file to 3D Printing service",$id("c3dp").style.visibility="hidden",e.style.visibility="hidden",$id("wtext").style.height="35px")))},!1),i.onreadystatechange=function(t){4==i.readyState&&(e.value=1,"OK"==i.responseText.substr(0,2)?window.location=i.responseText.substr(2):cancel_3dp||alert("Error preparing file for 3D Print"),$id("pwait").style.display="none")},i.open("POST","/3dprint/prepare_3dp.php",!0),i.setRequestHeader("Content-type","application/x-www-form-urlencoded"),i.send("vf="+JSON.stringify(geo_to_vf(mesh.geometry))+"&target=treatstock&units="+file_units+"&on="+model_filename)}}function do_resize(){var e=isNaN(window.innerHeight)?window.clientHeight:window.innerHeight,i=isNaN(window.innerWidth)?window.clientWidth:window.innerWidth;e=Math.min(e,i-150),$id("cjc").style.height=e-220+"px",$id("cjc").style.width=e-220+"px",$id("cjcwrap").style.height=e-220+"px",$id("cjcwrap").style.width=e-220+"px",$id("cjcdrag").style.top=(e-320)/2+"px",$id("cjcdrag").style.left=(e-570)/2+"px",$id("cjcpbar").style.top=(e-345)/2+"px",$id("cjcpbar").style.left=(e-570)/2+"px",$id("cjcproc").style.top=(e-345)/2+"px",$id("cjcproc").style.left=(e-570)/2+"px";var t=$id("cjcwrap").getBoundingClientRect().width,o=$id("cjcwrap").getBoundingClientRect().height;renderer.setSize(t-5,o-5)}function handleDragOver(e){waiting||(e.stopPropagation(),e.preventDefault(),e.dataTransfer.dropEffect="copy")}function handleFileDrop(e){if(e.stopPropagation(),e.preventDefault(),!waiting)if(e.dataTransfer.files.length>0){if(e.dataTransfer.files[0].size>max_file_size)return alert("File is too big - maximum allowed size is 35mb"),!1;if(!supported_file_type(e.dataTransfer.files[0].name))return alert("File type is not supported"),!1;read_file(e.dataTransfer.files[0])}else"string"==typeof e.dataTransfer.getData("Text")&&read_from_url(e.dataTransfer.getData("Text"))}function supported_file_type(e){switch(e.split(".").pop().toLowerCase()){case"stl":case"obj":case"vf":return!0;default:return!1}}function switch_view(e){$id("cjcdrag").style.display="drag"==e?"block":"none",$id("cjcpbar").style.display="pbar"==e?"block":"none",$id("cjcproc").style.display="proc"==e?"block":"none",$id("cjc").style.display="cjc"==e?"block":"none"}function after_error(){return switch_view("drag"),cancel_download=!1,waiting=!1,!1}function after_file_load(e,i){var t;try{t=parse_3d_file(e,i)}catch(o){t="Error parsing the file"}if("string"==typeof t)return alert(t),switch_view("drag"),void(waiting=!1);mesh_is_ready=!1,null!=mesh&&(scene.remove(mesh),mesh=null);var r=new THREE.Geometry;r.vertices=t.vertices,r.faces=t.faces,r.computeBoundingBox(),r.computeCentroids(),r.computeFaceNormals(),r.computeVertexNormals(),THREE.GeometryUtils.center(r),mesh=new THREE.Mesh(r,material),t.colors&&set_color($id("white_color"),"#FFFFFF"),update_mesh_color(),set_color(null,bg_color,!0),scene.add(mesh),mesh_is_ready=!0,directionalLight.position.x=2*r.boundingBox.min.y,directionalLight.position.y=2*r.boundingBox.min.y,directionalLight.position.z=2*r.boundingBox.max.z,pointLight.position.x=(r.boundingBox.min.y+r.boundingBox.max.y)/2,pointLight.position.y=(r.boundingBox.min.y+r.boundingBox.max.y)/2,pointLight.position.z=2*r.boundingBox.max.z,camera.position.set(0,0,Math.max(3*r.boundingBox.max.x,3*r.boundingBox.max.y,3*r.boundingBox.max.z)),controls.reset(),switch_view("cjc"),has_edges&&set_edges(!0),xsize=r.boundingBox.max.x-r.boundingBox.min.x,ysize=r.boundingBox.max.y-r.boundingBox.min.y,zsize=r.boundingBox.max.z-r.boundingBox.min.z,vol_area=calc_vol_and_area(r),vol=vol_area[0],area=vol_area[1],triangles_count=r.faces.length,model_filename=e,guess_file_units(xsize,ysize,zsize),recalc_units(),setTimeout(function(){$id("statswrap").style.width="280px"},500),waiting=!1}function set_color_by_name(e,i){switch(i=i||!1,e.toLowerCase()){case"black":set_color($id("black_color"),"#000000",i);break;case"white":set_color($id("white_color"),"#FFFFFF",i);break;case"blue":set_color($id("blue_color"),"#0000FF",i);break;case"green":set_color($id("green_color"),"#00FF00",i);break;case"red":set_color($id("red_color"),"#FF0000",i);break;case"yellow":set_color($id("yellow_color"),"#FFFF00",i);break;case"gray":set_color($id("gray_color"),"#909090",i);break;case"azure":set_color($id("azure_color"),"#00FFFF",i);break;case"pink":set_color($id("pink_color"),"#FF00FF",i);break;case"purple":set_color($id("purple_color"),"#703487",i);break;case"darkblue":set_color($id("darkblue_color"),"#102075",i);break;case"brown":set_color($id("brown_color"),"#654321",i);break;case"transparent":set_color($id("white_color"),"transparent",!0);break;default:/^#[0-9A-F]{6}$/i.test(e)&&set_color($id("white_color"),e,i)}}function set_color(e,i,t){if(t=t||!1)return bg_color=i,void("transparent"==i?renderer.setClearColor(0,0):renderer.setClearColor(i,1));c=$id("cpal").getElementsByTagName("div");for(var o=c.length;o--;)c[o]==e?c[o].style.border="2px solid #012101":c[o].style.border="2px solid transparent";mesh_color=i,update_mesh_color()}function update_mesh_color(){null!=mesh&&mesh.material.color.set(parseInt(mesh_color.substr(1),16))}function calc_vol_and_area(e){var i,t,o,r,a,n,s,c,l,d,_,u,p,f,g=e.faces.length,m=0,h=0;for(d=0;g>d;d++)i=e.vertices[e.faces[d].a].x,r=e.vertices[e.faces[d].a].y,s=e.vertices[e.faces[d].a].z,t=e.vertices[e.faces[d].b].x,a=e.vertices[e.faces[d].b].y,c=e.vertices[e.faces[d].b].z,o=e.vertices[e.faces[d].c].x,n=e.vertices[e.faces[d].c].y,l=e.vertices[e.faces[d].c].z,m+=-o*a*s+t*n*s+o*r*c-i*n*c-t*r*l+i*a*l,_=e.vertices[e.faces[d].a].distanceTo(e.vertices[e.faces[d].b]),u=e.vertices[e.faces[d].b].distanceTo(e.vertices[e.faces[d].c]),p=e.vertices[e.faces[d].c].distanceTo(e.vertices[e.faces[d].a]),f=(_+u+p)/2,h+=Math.sqrt(f*(f-_)*(f-u)*(f-p));return[Math.abs(m/6),h]}function numberWithCommas(e){var i=e.toString().split(".");return i[0]=i[0].replace(/\B(?=(\d{3})+(?!\d))/g,","),i.join(".")}function set_shading(e){2==e?material.wireframe=!0:(material.wireframe=!1,material.shading=1==e?THREE.SmoothShading:THREE.FlatShading,null!=mesh&&(mesh.geometry.normalsNeedUpdate=!0))}function view_example(e){download_from_local(e,e)}function reset(){waiting||(switch_view("drag"),remove_edges(),null!=mesh&&(scene.remove(mesh),mesh=null),$id("fileselect").value="")}function prepare_upload_file(){waiting||""!=$id("fileselect").value&&upload_file($id("fileselect").files[0])}function open_img(){var e=document.createElement("form");e.target="_blank",e.method="POST",e.action="/snap_img/";var i=document.createElement("input");i.type="text",i.name="img_data",i.value=renderer.domElement.toDataURL("image/png"),e.appendChild(i),document.body.appendChild(e),e.submit()}function setCookie(e,i,t){var o=new Date;o.setTime(o.getTime()+24*t*60*60*1e3);var r="expires="+o.toUTCString();document.cookie=e+"="+i+"; "+r}function getCookie(e){for(var i=e+"=",t=document.cookie.split(";"),o=0;o<t.length;o++){for(var r=t[o];" "==r.charAt(0);)r=r.substring(1);if(0==r.indexOf(i))return r.substring(i.length,r.length)}return""}function guess_file_units(e,i,t){file_units=1>e&&1>i&&1>t?2:1}function set_vol_and_size(e,i,t,o){}var need_browser=!1;"undefined"==typeof FileReader&&(need_browser=!0);var is_ie=!!window.MSStream,waiting=!1,mesh=null,material=new THREE.MeshLambertMaterial({color:9474192,overdraw:1,wireframe:!1,shading:THREE.FlatShading,vertexColors:THREE.FaceColors}),mesh_is_ready=!1;is_ie||(material.side=THREE.DoubleSide);var cancel_download=!1,cancel_3dp=!1,max_file_size=4e7,mesh_color="#909090",xsize=0,ysize=0,zsize=0,vol=0,area=0,triangles_count=0,model_filename="",view_units=1,file_units=1,bg_color=16777215,deg90=Math.PI/2,orientation="front",egh=null,has_edges=!1;
            var fstl = "{{ url('storage/' . $dataTypeContent->path) }}";
            function animate(){requestAnimationFrame(animate),renderer.render(scene,camera),controls.update()}function set_rotation(e){controls.autoRotate=e,animate()}function set_clean_mode(e){$id("titlet1").style.display=e?"none":"inline",$id("titlet2").style.display=e?"none":"inline",$id("pgt1").style.display=e?"none":"inline",$id("file_pbar").style.display=e?"none":"inline",$id("fcancel").style.display=e?"none":"inline",$id("prt1").style.display=e?"none":"inline"}function set_noborder(e){$id("cjcwrap").style.border=e?"none":"1px dashed #000000"}var scene=new THREE.Scene,is_webgl=webgl_Detector.webgl,renderer=is_webgl?new THREE.WebGLRenderer({preserveDrawingBuffer:!0,alpha:!0}):new THREE.CanvasRenderer,mesh=null,ambientLight=null,directionalLight=null,pointLight=null,camera=new THREE.PerspectiveCamera(45,1,.1,1e4);do_resize(),renderer.setClearColor(bg_color,1),$id("cjc").appendChild(renderer.domElement),camera.position.set(0,0,100),scene.add(camera),ambientLight=new THREE.AmbientLight(2105376),camera.add(ambientLight),directionalLight=new THREE.DirectionalLight(16777215,.75),directionalLight.position.x=1,directionalLight.position.y=1,directionalLight.position.z=2,directionalLight.position.normalize(),camera.add(directionalLight),pointLight=new THREE.PointLight(16777215,.3),pointLight.position.x=0,pointLight.position.y=-25,pointLight.position.z=10,camera.add(pointLight);var units_cookie=getCookie("units");view_units="in"==units_cookie?2:1;var controls=new THREE.OrbitControls(camera,renderer.domElement);is_webgl?controls.autoRotate=!0:($id("rrotate2").checked=!0,controls.autoRotate=!1),animate(),window.onresize=function(){do_resize()},$id("cjcwrap").addEventListener("dragover",handleDragOver,!1),$id("cjcwrap").addEventListener("drop",handleFileDrop,!1),view_example(fstl),need_browser&&alert("Your browser is too old and is not supported by this website. Please install a modern browser (Chrome is recommended)."),setTimeout(function(){$id("mainwrap").style.visibility="visible"},200);
        </script>
    @endif
@stop