@extends('voyager::master')

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> Viewing {{ ucfirst($dataType->display_name_singular) }}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered" style="padding-bottom:5px;">
                    <div class="panel-heading" style="border-bottom:0;">
                        <h3 class="panel-title">Name</h3>
                    </div>
                    <div class="panel-body" style="padding-top:0;">
                        <p>{{ $dataTypeContent->name }}</p>
                    </div>
                    <hr style="margin:0;">

                    <div class="panel-heading" style="border-bottom:0;">
                        <h3 class="panel-title">Email</h3>
                    </div>
                    <div class="panel-body" style="padding-top:0;">
                        <p>{{ $dataTypeContent->email }}</p>
                    </div>
                    <hr style="margin:0;">

                    <div class="panel-heading" style="border-bottom:0;">
                        <h3 class="panel-title">Role</h3>
                    </div>
                    <div class="panel-body" style="padding-top:0;">
                        <p>{{ $dataTypeContent->role->display_name }}</p>
                    </div>
                    <hr style="margin:0;">

                    <div class="panel-heading" style="border-bottom:0;">
                        <h3 class="panel-title">Avatar</h3>
                    </div>
                    <div class="panel-body" style="padding-top:0;">
                        <img style="max-width:640px" src="{!! Voyager::image($dataTypeContent->avatar) !!}">
                    </div>
                    <hr style="margin:0;">

                    <div class="panel-heading" style="border-bottom:0;">
                        <h3 class="panel-title">Created at</h3>
                    </div>
                    <div class="panel-body" style="padding-top:0;">
                        {{ \Carbon\Carbon::parse($dataTypeContent->created_at)->format('F jS, Y h:i A') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
