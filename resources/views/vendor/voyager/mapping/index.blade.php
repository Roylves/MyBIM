@extends('voyager::master')

@section('page_title', 'Upload')

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-tag"></i> Mapping
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    {{ Form::open(['action' => 'Voyager\MappingController@index', 'method' => 'GET']) }} 
                        <div class="panel-body">
                            <div class="form-group">
                                {{ Form::label('manufacturer_id', 'Manufacturer') }}
                                {{ Form::select('manufacturer_id', ['' => 'Select Manufacturer'] + App\Models\Manufacturer::pluck('name', 'id')->toArray(), request('manufacturer_id'), ['class' => 'form-control select2']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('type', 'Type') }}
                                {{ Form::select('type', ['' => 'Select Type', 'IMAGE' => 'IMAGE', 'STL' => 'STL', 'PLUGIN' => 'PLUGIN'], request('type'), ['class' => 'form-control select2']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('keyword', 'Keyword') }}
                                {{ Form::text('keyword', request('keyword'), ['class' => 'form-control']) }}
                            </div>

                            <div class="panel-footer">
                                <button type="submit" class="btn btn-primary save">Filter</button>
                            </div>
                        </div>
                    {{ Form::close() }}

                    {{ Form::open(['action' => 'Voyager\MappingController@store', 'method' => 'POST']) }} 
                        <div class="panel-body">
                            <div class="form-group">
                                {{ Form::label('object_id', 'Object') }}
                                {{ Form::select('object_id', ['' => 'Select Object'] + App\Models\Object::select('id', DB::raw('CONCAT(code, " - ", name) AS name'))->pluck('name', 'id')->toArray(), old('object_id'), ['class' => 'form-control select2']) }}
                            </div>

                            <label for="categories">Categories</label><br>
                            <a href="#" class="category-select-all">Select All</a> / <a href="#"  class="category-deselect-all">Deselect All</a>
                            <ul class="categories checkbox">
                                @foreach(App\Models\FileCategory::whereNull('parent_id')->get() as $category)
                                    <li>
                                        <input type="checkbox" id="{{ $category->slug . '-' . $category->id }}" class="category-group">
                                        <label for="{{ $category->slug . '-' . $category->id }}"><strong>{{ ucwords($category->name) }}</strong></label>
                                        <ul>
                                            @foreach($category->subCategories as $subCategory)
                                                <li>
                                                    <input type="checkbox" id="category-{{ $subCategory->id }}" name="category_ids[]" class="the-category" value="{{ $subCategory->id }}">
                                                    <label for="category-{{ $subCategory->id }}">{{ $subCategory->name }}</label>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @endforeach
                            </ul>

                            <div class="panel-footer">
                                <button type="submit" class="btn btn-primary save">Submit</button>
                            </div>
                        </div>

                        
                        <div class="panel-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="form-group">
                                <div class="table-responsive">
                                    <table role="presentation" class="table table-striped table-hover">
                                        <thead>
                                            <th><input type="checkbox" class="toggle"></th>
                                            <th>Thumbnail</th>
                                            <th>Path/Manufacturer</th>
                                            <th>Type</th>
                                        </thead>
                                        <tbody>
                                            @foreach($files as $file)
                                                <tr>
                                                    <td>{{ Form::checkbox('ids[]', $file->id, null, ['class' => 'control-form']) }}</td>
                                                    <td>
                                                        @if ($file->type == 'IMAGE')
                                                            <img src="{{ url('storage/' . $file->path) }}" style="width:100px">
                                                        @else
                                                            <p>No preview</p>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <p>
                                                            @if ($file->manufacturer_id)
                                                                <img height="19" src="{{ url('storage/' . $file->manufacturer->image) }}" alt="{{ $file->manufacturer->name }}">
                                                            @else
                                                                -
                                                            @endif
                                                        </p>
                                                        <p>{{ url('storage/' . $file->path) }}</p>
                                                    </td>
                                                    <td>{{ $file->type }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="pull-left">
                                        <div role="status" class="show-res" aria-live="polite">Showing {{ $files->firstItem() }} to {{ $files->lastItem() }} of {{ $files->total() }} entries</div>
                                    </div>
                                    <div class="pull-right">
                                        {{ $files->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script type="text/javascript">
    $(document).ready(function() {
        $(':checkbox[class="toggle"]').click (function () {
            $(':checkbox[name="id[]"]').prop('checked', this.checked);
        });

        $('.category-group').on('change', function(){
            $(this).siblings('ul').find("input[type='checkbox']").prop('checked', this.checked);
        });

        $('.category-select-all').on('click', function(){
            $('ul.categories').find("input[type='checkbox']").prop('checked', true);
            return false;
        });

        $('.category-deselect-all').on('click', function(){
            $('ul.categories').find("input[type='checkbox']").prop('checked', false);
            return false;
        });

        function parentChecked(){
            $('.category-group').each(function(){
                var allChecked = true;
                if ($(this).siblings('ul').find("input[type='checkbox']").length) {
                    $(this).siblings('ul').find("input[type='checkbox']").each(function(){
                        if (!this.checked) allChecked = false;
                    });
                    $(this).prop('checked', allChecked);
                }
            });
        }

        parentChecked();

        $('.the-category').on('change', function(){
            parentChecked();
        });
    });
    </script>
@stop