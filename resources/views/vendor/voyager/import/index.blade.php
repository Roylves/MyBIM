@extends('voyager::master')

@section('page_title', 'Import')

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-upload"></i> Import
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <form method="POST" enctype="multipart/form-data">
                        <div class="panel-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="form-group">
                                {{ Form::label('manufacturer', 'Manufacturer') }}
                                {{ Form::select('manufacturer_id', $manufacturers, null, ['class' => 'form-control', 'required']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('file', 'File') }}
                                {{ Form::file('file', ['required']) }}
                            </div>

                            <div class="form-group">
                                <span class="icon voyager-download"></i><a href="{{ url('/storage/format.xls') }}" target="_blank"> Download sample format</a></span>
                            </div>
                        </div>

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">Upload</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop