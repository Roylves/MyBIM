@extends('voyager::master')

@section('page_title', 'Upload')

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-upload"></i> Upload
    </h1>
@stop

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/jquploader/css/blueimp-gallery.min.css') }}">
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-heading">
                        <h3 class="panel-title">New Upload</h3>
                    </div>

                    <form id="fileupload" method="POST" enctype="multipart/form-data">
                        <div class="panel-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="form-group">
                                {{ Form::label('manufacturer', 'Manufacturer') }}
                                {{ Form::select('manufacturer_id', $manufacturers, null, ['class' => 'form-control', 'required']) }}
                            </div>

                            <div class="form-group">
                                <div class="fileupload-buttonbar">
                                    <span class="btn btn-success fileinput-button" onclick="document.getElementById('files').click()">
                                        <i class="glyphicon glyphicon-plus"></i>
                                        <span>Add files...</span>
                                        <input type="file" name="files[]" id="files" multiple style="display: none">
                                    </span>
                                    <button type="submit" class="btn btn-primary start">
                                        <i class="glyphicon glyphicon-upload"></i>
                                        <span>Start upload</span>
                                    </button>
                                    <button type="reset" class="btn btn-warning cancel">
                                        <i class="glyphicon glyphicon-ban-circle"></i>
                                        <span>Cancel upload</span>
                                    </button>
                                    <button type="button" class="btn btn-danger delete">
                                        <i class="glyphicon glyphicon-trash"></i>
                                        <span>Delete</span>
                                    </button>

                                    <!-- <input type="checkbox" class="toggle"> -->
                                    <span class="fileupload-process"></span>
                                    <div class="fileupload-progress fade">
                                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                            <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                        </div>
                                        <div class="progress-extended">&nbsp;</div>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table role="presentation" class="table table-striped table-hover">
                                        <thead>
                                            <th><input type="checkbox" class="toggle"></th>
                                            <th>Thumbnail</th>
                                            <th>Name</th>
                                            <th>Size</th>
                                            <th>Action</th>
                                        </thead>
                                        <tbody class="files"></tbody>
                                    </table>
                                </div>

                                <!-- The blueimp Gallery widget -->
                                <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
                                    <div class="slides"></div>
                                    <h3 class="title"></h3>
                                    <a class="prev">‹</a>
                                    <a class="next">›</a>
                                    <a class="close">×</a>
                                    <a class="play-pause"></a>
                                    <ol class="indicator"></ol>
                                </div>
                                <!-- The template to display files available for upload -->
                                <script id="template-upload" type="text/x-tmpl">
                                {% for (var i=0, file; file=o.files[i]; i++) { %}
                                    <tr class="template-upload fade">
                                        <td></td>
                                        <td>
                                            <span class="preview"></span>
                                        </td>
                                        <td>
                                            <p class="name">{%=file.name%}</p>
                                            <strong class="error text-danger"></strong>
                                        </td>
                                        <td>
                                            <p class="size">Processing...</p>
                                            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
                                        </td>
                                        <td>
                                            {% if (!i && !o.options.autoUpload) { %}
                                                <button class="btn btn-primary start" disabled>
                                                    <i class="glyphicon glyphicon-upload"></i>
                                                    <span>Start</span>
                                                </button>
                                            {% } %}
                                            {% if (!i) { %}
                                                <button class="btn btn-warning cancel">
                                                    <i class="glyphicon glyphicon-ban-circle"></i>
                                                    <span>Cancel</span>
                                                </button>
                                            {% } %}
                                        </td>
                                     </tr>
                                {% } %}
                                </script>
                                <!-- The template to display files available for download -->
                                <script id="template-download" type="text/x-tmpl">
                                {% for (var i=0, file; file=o.files[i]; i++) { %}
                                    <tr class="template-download fade">
                                        <td><input type="checkbox" name="delete" value="1" class="toggle"></td>
                                        <td>
                                            <span class="preview">
                                                {% if (file.thumbnailUrl && file.type == 'IMAGE') { %}
                                                    <img width="80" height="80" src="{%=file.thumbnailUrl%}">
                                                {% } else { %}
                                                    <span>No preview</span>
                                                {% } %}
                                            </span>
                                        </td>
                                        <td>
                                            <p class="name">
                                                {% if (file.url) { %}
                                                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                                                {% } else { %}
                                                    <span>{%=file.name%}</span>
                                                {% } %}
                                            </p>
                                            {% if (file.error) { %}
                                                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                                            {% } %}
                                        </td>
                                        <td>
                                            <span class="size">{%=o.formatFileSize(file.size)%}</span>
                                        </td>
                                        <td>
                                            {% if (file.deleteUrl) { %}
                                                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                    <span>Delete</span>
                                                </button>
                                                <!-- <input type="checkbox" name="delete" value="1" class="toggle"> -->
                                            {% } else { %}
                                                <button class="btn btn-warning cancel">
                                                    <i class="glyphicon glyphicon-ban-circle"></i>
                                                    <span>Cancel</span>
                                                </button>
                                            {% } %}
                                        </td>
                                    </tr>
                                {% } %}
                                </script>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script src="{{ asset('vendor/jquploader/js/vendor/jquery.ui.widget.js') }}"></script>
    <script src="{{ asset('vendor/jquploader/js/tmpl.min.js') }}"></script>
    <script src="{{ asset('vendor/jquploader/js/load-image.all.min.js') }}"></script>
    <script src="{{ asset('vendor/jquploader/js/canvas-to-blob.min.js') }}"></script>
    <script src="{{ asset('vendor/jquploader/js/jquery.blueimp-gallery.min.js') }}"></script>
    <script src="{{ asset('vendor/jquploader/js/jquery.iframe-transport.js') }}"></script>
    <script src="{{ asset('vendor/jquploader/js/jquery.fileupload.js') }}"></script>dor
    <script src="{{ asset('vendor/jquploader/js/jquery.fileupload-process.js') }}"></script>
    <script src="{{ asset('vendor/jquploader/js/jquery.fileupload-image.js') }}"></script>
    <script src="{{ asset('vendor/jquploader/js/jquery.fileupload-audio.js') }}"></script>
    <script src="{{ asset('vendor/jquploader/js/jquery.fileupload-video.js') }}"></script>
    <script src="{{ asset('vendor/jquploader/js/jquery.fileupload-validate.js') }}"></script>
    <script src="{{ asset('vendor/jquploader/js/jquery.fileupload-ui.js') }}"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $(':checkbox[class="toggle"]').click (function () {
            $(':checkbox[name="delete"]').prop('checked', this.checked);
        });
        var fileUploadOption = {
            url: "{{ action('Voyager\UploadController@store') }}"
        };
        // init file upload
        $('#fileupload').fileupload(fileUploadOption);
        // event listener
        $('#fileupload').bind('fileuploadadd', function (e, data) {
            manufacturer_id = $('select[name="manufacturer_id"]').val();
            data.formData = {manufacturer_id: manufacturer_id, "_token": "{{ csrf_token() }}"};
        }).bind('fileuploaddestroy', function (e, data) {
            data.formData = {"_method": "DELETE", "_token": "{{ csrf_token() }}"};
        });
    });
    </script>
@stop