<?php

return [

    'record'    => 'Record|Records',
    'file'      => 'File|Files',
    'object'    => 'Object|Objects',
    'plugin'    => 'Plugin|Plugins',

];