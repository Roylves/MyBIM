<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin'], function () {

    Route::resource('objects/import', 'Voyager\ImportController');
    Route::resource('files/mapping', 'Voyager\MappingController');
    Route::resource('files/upload', 'Voyager\UploadController');
    
    Route::get('objects/json_list', 'Voyager\ObjectController@getJsonList');
    Route::get('categories/json_list', 'Voyager\CategoryController@getJsonList');

    Voyager::routes();
});

Route::get('login/{provider}', 'SocialAuthController@redirect');
Route::get('login/{provider}/callback', 'SocialAuthController@callback');

Route::get('logout', 'Auth\LoginController@logout');
Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::put('profile', 'ProfileController@updateProfile');
    Route::get('profile', 'ProfileController@index');
    Route::get('activities', 'ProfileController@activities');
    Route::get('ratings', 'ProfileController@ratings');
    Route::get('favorites', 'ProfileController@favorites');
    Route::get('download/{fileId}', 'ObjectController@download');

    Route::prefix('api')->group(function () {
        Route::post('objects/{objectId}/favorite', 'ObjectController@favorite');
        Route::post('objects/{objectId}/rate', 'ObjectController@rate');
        Route::get('objects', 'ObjectController@index');
    });
});

Route::prefix('api')->group(function () {
    Route::get('objects/most_rated', 'ObjectController@mostRated');
    Route::get('objects/most_viewed', 'ObjectController@mostViewed');
    Route::get('objects/most_downloaded', 'ObjectController@mostDownloaded');
    Route::get('objects/latest', 'ObjectController@latest');
    Route::get('plugin', 'ObjectController@plugin');
    Route::get('session', 'ObjectController@session');
});

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
Route::get('plugin', 'HomeController@plugin');
Route::get('{manufacturerSlug}/{objectSlug}', 'ObjectController@getBySlug');
Route::get('{manufacturerSlug}', 'HomeController@manufacturer');
Route::get('/', 'HomeController@index');
