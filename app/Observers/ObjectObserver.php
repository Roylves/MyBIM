<?php

namespace App\Observers;

use App\Models\Object;

class ObjectObserver
{
    /**
     * Listen to the Object saving event.
     *
     * @param  Object  $object
     * @return void
     */
    public function saving(Object $object)
    {
        if ($object->system_code) {
            return $object;
        }
        if (!$object->manufacturer_id) {
            return $object;
        }

        if (!$object->code) {
            return $object;
        }

        if (!$object->properties) {
            return $object;
        }

        $manufacturer = $object->manufacturer;
        $nextSequence = $manufacturer->last_sequence + 1;
        $manufacturer->last_sequence = $nextSequence;
        $manufacturer->save();

        $object->system_code = $object->code . '/' . strtoupper($object->manufacturer->name) . '/' . date("Ymd") . '/' . sprintf("%06d", $nextSequence);
        $object->save();

        return $object;
    }
}