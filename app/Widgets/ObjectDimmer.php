<?php

namespace App\Widgets;

use App\Models\Object;
use Arrilot\Widgets\AbstractWidget;
use TCG\Voyager\Facades\Voyager;

class ObjectDimmer extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = number_format(Object::count());
        $string = $count == 1 ? 'object' : 'objects';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-file-text',
            'title'  => "{$count} {$string}",
            'text'   => null,
            'button' => [
                'text' => 'View all objects',
                'link' => action('Voyager\ObjectController@index'),
            ]
        ]));
    }
}
