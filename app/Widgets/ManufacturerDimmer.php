<?php

namespace App\Widgets;

use App\Models\Manufacturer;
use Arrilot\Widgets\AbstractWidget;
use TCG\Voyager\Facades\Voyager;

class ManufacturerDimmer extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = number_format(Manufacturer::count());
        $string = $count == 1 ? 'manufacturer' : 'manufacturers';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-company',
            'title'  => "{$count} {$string}",
            'text'   => null,
            'button' => [
                'text' => 'View all manufacturers',
                'link' => route('voyager.manufacturers.index'),
            ]
        ]));
    }
}
