<?php

namespace App\Widgets;

use App\Models\File;
use Arrilot\Widgets\AbstractWidget;
use TCG\Voyager\Facades\Voyager;

class FileDimmer extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = number_format(File::count());
        $string = $count == 1 ? 'file' : 'files';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-file-text',
            'title'  => "{$count} {$string}",
            'text'   => null,
            'button' => [
                'text' => 'View all files',
                'link' => action('Voyager\FileController@index'),
            ]
        ]));
    }
}
