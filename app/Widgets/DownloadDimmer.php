<?php

namespace App\Widgets;

use App\Models\File;
use Arrilot\Widgets\AbstractWidget;
use TCG\Voyager\Facades\Voyager;

class DownloadDimmer extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = number_format(File::sum('download_count'));
        $string = $count == 1 ? 'download' : 'downloads';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-download',
            'title'  => "{$count} {$string}",
            'text'   => null,
            'button' => [
                'text' => 'View all downloads',
                'link' => action('Voyager\DownloadController@index'),
            ]
        ]));
    }
}
