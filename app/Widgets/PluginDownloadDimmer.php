<?php

namespace App\Widgets;

use App\Models\File;
use Arrilot\Widgets\AbstractWidget;
use TCG\Voyager\Facades\Voyager;

class PluginDownloadDimmer extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = number_format(File::where('object_id', 830)->sum('download_count'));
        $string = $count == 1 ? 'plugin download' : 'plugin downloads';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-download',
            'title'  => "{$count} {$string}",
            'text'   => null,
            'button' => [
                'text' => 'View all plugin downloads',
                'link' => action('Voyager\DownloadController@index'),
            ]
        ]));
    }
}
