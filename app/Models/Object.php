<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use ChristianKuri\LaravelFavorite\Traits\Favoriteable;
use Trexology\ReviewRateable\Contracts\ReviewRateable;
use Trexology\ReviewRateable\Traits\ReviewRateable as ReviewRateableTrait;
use Auth;

class Object extends Model implements AuditableContract, ReviewRateable
{
    use Auditable, Favoriteable, ReviewRateableTrait, Sluggable, SoftDeletes;

    const STATUS_PUBLISHED = 'PUBLISHED';
    const STATUS_DRAFT = 'DRAFT';
    const STATUS_PENDING = 'PENDING';

    public static $statuses = [self::STATUS_PUBLISHED, self::STATUS_DRAFT, self::STATUS_PENDING];    

    protected $guarded = [];

    protected $casts = [
        'properties' => 'json',
    ];

    public function manufacturerId()
    {
        return $this->belongsTo(Manufacturer::class);
    }

    public function manufacturer()
    {
        return $this->belongsTo(Manufacturer::class);
    }

    public function categoryId(){
        return $this->belongsToMany(Category::class);
    }    

    public function scopePublished(Builder $query)
    {
        return $query->where('status', '=', static::STATUS_PUBLISHED);
    } 

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    } 
    
    public function image()
    {
        return $this->hasMany(File::class, 'object_id')
            ->whereType('IMAGE');
    }

    public function stl()
    {
        return $this->hasMany(File::class, 'object_id')
            ->whereType('STL');
    }

    public function files()
    {
        return $this->hasMany(File::class, 'object_id')
            ->whereType('PLUGIN')->orderBy('updated_at', 'DESC');
    }

    public function allFiles()
    {
        return $this->hasMany(File::class, 'object_id');
    }

    public function download_count()
    {
        $files = $this->allFiles;
        $count = 0;
        foreach ($files as $file) {
            $count += $file->download_count;
        }

        return $count;
    }    

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_object', 'object_id', 'category_id')->withTimestamps();
    }

    public function platforms()
    {
        return CategoryFile::join('file_categories', 'file_categories.id', '=', 'category_file.category_id')
            ->join('files', 'files.id', '=', 'category_file.file_id')
            ->where('file_categories.parent_id', 1)
            ->where('file_categories.status', FileCategory::STATUS_PUBLISHED)
            ->where('files.object_id', $this->id)
            ->where('files.type', 'PLUGIN')
            ->get();
    }
}