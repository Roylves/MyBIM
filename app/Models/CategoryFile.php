<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class CategoryFile extends Model implements AuditableContract
{
    use Auditable;

    protected $table = 'category_file';
}