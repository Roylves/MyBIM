<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Regulus\ActivityLog\Models\Activity;

class File extends Model implements AuditableContract
{
    use Auditable, SoftDeletes;

    protected $guarded = [];

    public function incrementDownloadCount()
    {
        $description = 'downloaded_item';
        $item = 'SPAL|labels.file';

        if ($this->isPlugin() === true) {
            $description = 'downloaded_plugin';
            $item = 'SPAL|labels.plugin';
        }

        Activity::log([
            'contentId'   => $this->id,
            'contentType' => 'File',
            'description' => [
                $description, [
                    'number'    => 1,
                    'item'      => $item
                ]
            ]
        ]);
        
        $this->download_count++;
        return $this->save();
    }

    public function manufacturer()
    {
        return $this->belongsTo(Manufacturer::class);
    }

    public function object()
    {
        return $this->belongsTo(Object::class)->withTrashed();
    }

    public function categories()
    {
        return $this->belongsToMany(FileCategory::class, 'category_file', 'file_id', 'category_id')->withTimestamps();
    }  

    public function scopeFeatured(Builder $query)
    {
        return $query->where('featured', 1);
    }

    public function scopeIsPlugin(Builder $query)
    {
        $fileCategories = $this->categories->pluck('id');
        $pluginCategoryIds = FileCategory::pluginCategoryIds()->toArray();
        $isPlugin = false;

        // check for file category (plugin)
        foreach ($fileCategories as $fileCategory) {
            if (in_array($fileCategory, $pluginCategoryIds)) {
                $isPlugin = true;
                continue;
            }
        }

        return $isPlugin;
    }
}