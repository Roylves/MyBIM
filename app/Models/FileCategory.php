<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class FileCategory extends Model implements AuditableContract
{
    use Auditable, SoftDeletes;

    const STATUS_PUBLISHED = 'PUBLISHED';
    const STATUS_DRAFT = 'DRAFT';
    const STATUS_PENDING = 'PENDING';

    public static $statuses = [self::STATUS_PUBLISHED, self::STATUS_DRAFT, self::STATUS_PENDING];    

    protected $table = 'file_categories';

    protected $fillable = ['slug', 'name'];

    public function parentId()
    {
        return $this->belongsTo(self::class);
    }

    public function parent()
    {
        return $this->belongsTo(self::class);
    }

    public function subCategories()
    {
        return $this->hasMany(self::class, 'parent_id')
            ->orderBy('name', 'ASC');
    }    

    public function scopePublished(Builder $query)
    {
        return $query->where('status', '=', static::STATUS_PUBLISHED);
    }

    public function scopePluginCategoryIds(Builder $query)
    {
        return $query->where('parent_id', 1)->pluck('id');
    }    
}