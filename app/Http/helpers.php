<?php

function get_unit($property)
{
    $properties = Cache::remember('property-unit', '60', function () {
        return App\Models\Property::pluck('unit', 'name');
    });
    
    return $properties[$property];
}