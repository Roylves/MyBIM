<?php

namespace App\Http\Controllers\Voyager;

use App\Models\File;
use App\Models\Manufacturer;
use App\Models\Object;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBreadController as BaseVoyagerBreadController;

class FileController extends BaseVoyagerBreadController
{
    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        Voyager::canOrFail('browse_'.$dataType->name);

        $query = File::query();

        if ($request->has('manufacturer_id')) {
            $query->where('manufacturer_id', $request->get('manufacturer_id'));
        }

        if ($request->has('type')) {
            $query->where('type', $request->get('type'));
        }

        if ($request->has('keyword')) {
            $query->where('path', 'like', "%{$request->get('keyword')}%");
        }

        $dataTypeContent = $query->orderBy('id', 'desc')->paginate(15);

        return view('voyager::files.browse', compact('dataType', 'dataTypeContent'));
    }

    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        Voyager::canOrFail('add_'.$dataType->name);

        $this->validate($request, [
            'object_id' => 'required',
            'type' => 'required',
            'file' => 'required'
        ]);

        $newObject = $this->insertUpdate($request);

        return redirect()
            ->route("voyager.{$dataType->slug}.edit", ['id' => $newObject->id])
            ->with([
                'message'    => "Successfully Added New {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        Voyager::canOrFail('edit_'.$dataType->name);

        $this->validate($request, [
            'object_id' => 'required',
            'type' => 'required'
        ]);

        $file = $this->insertUpdate($request, $id);

        return redirect()
            ->route("voyager.{$dataType->slug}.edit", ['id' => $file->id])
            ->with([
                'message'    => "Successfully Updated {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function insertUpdate($request, $id = null)
    {
        $object = Object::findOrFail($request->get('object_id'));

        $file = $id ? File::findOrFail($id) : new File();
        $file->manufacturer_id = $object->manufacturer->id;
        $file->object_id = $object->id;
        $file->mime = $request->get('mime');
        $file->size = $request->get('size');
        $file->type = $request->get('type');
        $file->save();
        $file->categories()->sync($request->get('category_ids'));

        if ($request->hasFile('file')) {
            if ($file->path) {
                // can't replicate old to new File to maintain the Id for link
                $oldFile = $file->replicate();
                $oldFile->save();
                $oldFile->delete();
            }
            $this->processImageUpload($request, 'file', $file);
        }

        return $file;
    }

    public function processImageUpload(Request $request, $inputName, $currentFile)
    {
        $file = $request->file($inputName);
        $filename = time() . '_' . $file->getClientOriginalName();

        $manufacturer = $currentFile->object->manufacturer;
        if (!$manufacturer) {
            $path = 'objects/unknown/';
        } else {
            $path = 'objects/' . $manufacturer->slug . '/' . $currentFile->object->code . '/';
        }
        $fullPath = $path . $filename;
        $request->file($inputName)->storeAs(
                        $path,
                        $filename,
                        config('voyager.storage.disk', 'public')
                    );

        $currentFile->path = $fullPath;
        $currentFile->mime = $file->getClientMimeType();
        $currentFile->size = $file->getClientSize();
        $currentFile->save();

        return $currentFile;
    }

    public function getJsonList(Request $request)
    {
        $query = Object::query();

        if ($request->has('status')) {
           $query->where('status', $request->input('status')); 
        }

        return $query->select('id', DB::raw('CONCAT(code, " - ", name) name'))->pluck('name', 'id');
    }    
}