<?php

namespace App\Http\Controllers\Voyager;

use App\Models\File;
use App\Models\Manufacturer;
use App\Models\Object;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBreadController as BaseVoyagerBreadController;

class ObjectController extends BaseVoyagerBreadController
{
    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        Voyager::canOrFail('browse_'.$dataType->name);

        $query = Object::query();

        if ($request->has('manufacturer_id')) {
            $query->where('manufacturer_id', $request->get('manufacturer_id'));
        }

        if ($request->has('status')) {
            $query->where('status', $request->get('status'));
        }

        if ($request->has('keyword')) {
            $query->where('name', 'like', "%{$request->get('keyword')}%");
            $query->orWhere('slug', 'like', "%{$request->get('keyword')}%");
            $query->orWhere('code', 'like', "%{$request->get('keyword')}%");
            $query->orWhere('system_code', 'like', "%{$request->get('keyword')}%");
            $query->orWhere('description', 'like', "%{$request->get('keyword')}%");
            $query->orWhere('properties', 'like', "%{$request->get('keyword')}%");
        }

        $dataTypeContent = $query->orderBy('id', 'desc')->paginate(15);

        return view('voyager::objects.browse', compact('dataType', 'dataTypeContent'));
    }

    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        Voyager::canOrFail('add_'.$dataType->name);

        $this->validate($request, [
            'manufacturer_id' => 'required',
            'name' => 'required',
            'slug' => 'required'
        ]);

        $newObject = $this->insertUpdate($request);

        return redirect()
            ->route("voyager.{$dataType->slug}.edit", ['id' => $newObject->id])
            ->with([
                'message'    => "Successfully Added New {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        Voyager::canOrFail('edit_'.$dataType->name);

        $this->validate($request, [
            'manufacturer_id' => 'required',
            'name' => 'required',
            'slug' => 'required'
        ]);

        $this->insertUpdate($request, $id);

        return redirect()
            ->route("voyager.{$dataType->slug}.edit", ['id' => $id])
            ->with([
                'message'    => "Successfully Updated {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function insertUpdate($request, $id = null)
    {
        $object = $id ? Object::findOrFail($id) : new Object();
        $object->manufacturer_id = $request->get('manufacturer_id');
        $object->name = $request->get('name');
        $object->slug = $request->get('slug');
        $object->code = $request->has('code') ? $request->get('code') : null;
        $object->description = $request->has('description') ? $request->get('description') : null;
        
        $property_names = $request->get('property_name');
        $property_values = $request->get('property_value');

        $properties = [];
        foreach ($property_names as $key => $name) {
            if ($property_values[$key] != '') {
                $properties[$name] = $property_values[$key];
            }
        }

        $object->properties = count($properties) ? $properties : null;

        $object->status = $request->get('status');
        $object->save();
        $object->categories()->sync($request->get('category_ids'));

        if ($request->hasFile('image')) {
            if ($object->image->count()) {
                if ($object->image->first()->path) {
                    // can't replicate old to new File to maintain the Id for link
                    $oldFile = $object->image->first()->replicate();
                    $oldFile->save();
                    $oldFile->delete();
                }
            }
            $this->processImageUpload($request, 'image', $object->image->first());
        }

        if ($request->hasFile('stl')) {
            if ($object->stl->first()->path) {
                // can't replicate old to new File to maintain the Id for link
                $oldFile = $object->stl->first()->replicate();
                $oldFile->save();
                $oldFile->delete();
            }
            $this->processImageUpload($request, 'stl', $object->stl->first());
        }

        if ($request->hasFile('plugin')) {
            // keep adding file for Plugin
            $this->processImageUpload($request, 'plugin', $object->files->first());
        }        

        return $object;
    }

    public function processImageUpload(Request $request, $inputName, $currentFile)
    {
        $file = $request->file($inputName);
        $filename = time() . '_' . $file->getClientOriginalName();

        $manufacturer = Manufacturer::findOrFail($request->get('manufacturer_id'));
        $path = 'objects/' . $manufacturer->slug . '/' . $currentFile->object->code . '/';
        $fullPath = $path . $filename;
        $request->file($inputName)->storeAs(
                        $path,
                        $filename,
                        config('voyager.storage.disk', 'public')
                    );

        // @todo need to delete previous one? for plugin need to keep previous one
        // if ($currentFile->object->{$inputName}->count()) {
        //     $currentFile->object->{$inputName}->first()->delete();
        // }

        if ($inputName == 'plugin') {
            $newFile = new File();
            $newFile->manufacturer_id = $request->get('manufacturer_id');
            $newFile->object_id = $currentFile->object->id;
            $newFile->path = $fullPath;
            $newFile->mime = $file->getClientMimeType();
            $newFile->size = $file->getClientSize();
            $newFile->type = strtoupper($inputName);
            $newFile->save();

            return $newFile;
        } else {
            $currentFile->manufacturer_id = $request->get('manufacturer_id');
            $currentFile->object_id = $currentFile->object->id;
            $currentFile->path = $fullPath;
            $currentFile->mime = $file->getClientMimeType();
            $currentFile->size = $file->getClientSize();
            $currentFile->type = strtoupper($inputName);
            $currentFile->save();

            return $currentFile;
        }
    }

    public function getJsonList(Request $request)
    {
        $query = Object::query();

        if ($request->has('status')) {
           $query->where('status', $request->input('status')); 
        }

        return $query->select('id', DB::raw('CONCAT(code, " - ", name) name'))->pluck('name', 'id');
    }    
}