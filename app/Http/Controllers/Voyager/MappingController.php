<?php

namespace App\Http\Controllers\Voyager;

use App\Models\File;
use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBreadController as BaseVoyagerBreadController;

class MappingController extends BaseVoyagerBreadController
{
    public function index(Request $request)
    {
        $query = File::query();

        if ($request->has('manufacturer_id')) {
            $query->where('manufacturer_id', $request->get('manufacturer_id'));
        }

        if ($request->has('type')) {
            $query->where('type', $request->get('type'));
        }

        if ($request->has('keyword')) {
            $query->where('path', 'like', "%{$request->get('keyword')}%");
        }

        $files = $query->whereNull('object_id')->orderBy('id', 'desc')->paginate(15);

        return view('voyager::mapping.index', compact('files'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required'
        ]);

        foreach ($request->get('ids') as $id) {

            $file = File::findOrFail($id);
            if ($request->has('object_id')) {
                $file->object_id = $request->get('object_id');
            }
            $file->save();

            if ($request->has('category_ids')) {
                $file->categories()->sync($request->get('category_ids'));
            }
        }

        return redirect()
            ->action('Voyager\MappingController@index')
            ->with([
                'message'    => "Successfully Mapped File(s)",
                'alert-type' => 'success',
            ]);
    }
}