<?php

namespace App\Http\Controllers\Voyager;

use App\Models\File;
use App\Models\Manufacturer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use stdClass;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBreadController as BaseVoyagerBreadController;

class UploadController extends BaseVoyagerBreadController
{
    public function index(Request $request)
    {   
        $manufacturers = Manufacturer::pluck('name', 'id');

        return view('voyager::upload.index', compact('manufacturers'));
    }

    public function store(Request $request)
    {
        if ($request->hasFile('files')) {
            $manufacturer = Manufacturer::findOrFail($request->input('manufacturer_id'));
            $uploadPath = 'storage/objects/' . $manufacturer->slug . '/';

            foreach ($request->file('files') as $file) {
                $filename = time() . '_' . $file->getClientOriginalName();
                
                $path = 'objects/' . $manufacturer->slug . '/';
                $fullPath = $path . $filename;
                $file->storeAs(
                                $path,
                                $filename,
                                config('voyager.storage.disk', 'public')
                            );
                
                $newFile = new File();
                $newFile->manufacturer_id = $request->input('manufacturer_id');
                $newFile->path = $fullPath;
                $newFile->mime = $file->getClientMimeType();
                $newFile->size = $file->getClientSize();
                $type =  'NONE';
                if ($file->getClientOriginalExtension() == 'png') {
                    $type = 'IMAGE';
                } elseif ($file->getClientOriginalExtension() == 'stl') {
                    $type = 'STL';
                } else {
                    $type = 'PLUGIN';
                }
                $newFile->type = $type;
                $newFile->save();

                $responseFile = new stdClass();
                $responseFile->id = $newFile->id;
                $responseFile->name = $filename;
                $responseFile->size = $newFile->size;
                $responseFile->type = $newFile->type;
                $responseFile->url = url('storage/' . $newFile->path);
                $responseFile->thumbnailUrl = url('storage/'. $newFile->path);
                $responseFile->deleteUrl = action('Voyager\UploadController@destroy', $newFile->id);
                $responseFile->deleteType = 'DELETE';
            }

            return response()->json(['files'=> [$responseFile]], 200);
        }
    }

    public function destroy(Request $request, $id)
    {
        $file = File::findOrFail($id);
        $file->delete();

        return response()->json(true, 200);
    }
}