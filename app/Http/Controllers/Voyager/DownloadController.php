<?php

namespace App\Http\Controllers\Voyager;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Regulus\ActivityLog\Models\Activity;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBreadController as BaseVoyagerBreadController;

class DownloadController extends BaseVoyagerBreadController
{
    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        Voyager::canOrFail('browse_'.$dataType->name);

        $dataTypeContent = Activity::where('description', 'like', '%downloaded_%')->orderBy('id', 'desc')->paginate(15);
        
        return view('voyager::downloads.browse', compact('dataType', 'dataTypeContent'));
    }
}