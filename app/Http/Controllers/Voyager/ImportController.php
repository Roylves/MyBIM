<?php

namespace App\Http\Controllers\Voyager;

use App\Models\Category;
use App\Models\Manufacturer;
use App\Models\Object;
use App\Models\Property;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Excel;
use Exception;
use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBreadController as BaseVoyagerBreadController;

class ImportController extends BaseVoyagerBreadController
{
    public function index(Request $request)
    {   
        $manufacturers = Manufacturer::pluck('name', 'id');
        
        return view('voyager::import.index', compact('manufacturers'));
    }

    public function store(Request $request)
    {
        $errors = [];

        $this->validate($request, [
            'file' => 'required|mimes:csv,xlsx,xls,txt',
        ]);

        $manufacturer = Manufacturer::findOrFail($request->input('manufacturer_id'));
        $properties = Property::pluck('name');
        $categories = Category::pluck('id', 'name');
        // dd($categories);

        $name = Carbon::now()->format('YmdHis') . '.' . $request->file->extension();
        $request->file->storeAs('public/uploads/excel/' . $manufacturer->slug, $name);

        $objects = Excel::load('storage/uploads/excel/' . $manufacturer->slug . '/' . $name)->get();

        DB::beginTransaction();

        try {
            $count = 0;
            foreach ($objects as $key => $object) {
                $newObject = new Object();
                $newObject->manufacturer_id = $manufacturer->id;
                $newObject->name = $object->name;
                $newObject->code = $object->code;
                $newObject->system_code = null;
                $newObject->description = $object->description;

                $newObjectProperties = [];
                foreach ($properties as $propertyName) {
                    if (isset($object->$propertyName)) {
                        $newObjectProperties[$propertyName] = $object->$propertyName;
                    }
                }

                $newObjectCategories = explode(',', $object->categories);
                $newObjectCategoryIds = [];
                foreach ($newObjectCategories as $categoryName) {
                    if ($categoryName) {
                        $newObjectCategoryIds[] = $categories[$categoryName];
                    }
                }

                $newObject->properties = count($newObjectProperties) ? $newObjectProperties : null;
                $newObject->status = 'DRAFT';
                $newObject->save();
                $newObject->categories()->sync($newObjectCategoryIds);
            }
            if (count($errors) > 0) {
                return redirect()->action('Voyager\ImportController@index')->withErrors('Object(s) failed to import1. ' . $error->getMessage());
            }

            DB::commit();
            return redirect()->action('Voyager\ImportController@index')->with(['message' => 'Object(s) successfully imported', 'alert-type' => 'success']);

        } catch (Exception $error) {
            DB::rollback();
            return redirect()->action('Voyager\ImportController@index')->withErrors('Object(s) failed to import2. ' . $error->getMessage());
        } 
    }
}