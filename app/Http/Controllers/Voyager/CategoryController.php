<?php

namespace App\Http\Controllers\Voyager;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBreadController as BaseVoyagerBreadController;

class CategoryController extends BaseVoyagerBreadController
{
    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        Voyager::canOrFail('browse_'.$dataType->name);

        $dataTypeContent = Category::orderBy('id', 'desc')->get();
        
        return view('voyager::categories.browse', compact('dataType', 'dataTypeContent'));
    }

    public function getJsonList(Request $request)
    {
        $query = Category::query();

        if ($request->has('parent_id')) {
           $query->where('parent_id', $request->input('parent_id')); 
        }

        return $query->pluck('name', 'id');
    }
}