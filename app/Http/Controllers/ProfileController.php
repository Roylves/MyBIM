<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Regulus\ActivityLog\Models\Activity;
use Trexology\ReviewRateable\Models\Rating;
use ChristianKuri\LaravelFavorite\Models\Favorite;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('frontend.v2.profile.index');
    }

    public function updateProfile(Request $request)
    {
        $user = Auth::user();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        if ($request->has('password')) {
            $user->password = bcrypt($request->get('password'));
        }
        $user->profession_id = $request->get('profession_id');
        if ($user->save()) {
            Activity::log([
                'contentId'   => Auth::id(),
                'contentType' => 'User',
                'description' => [
                    'updated_profile', []
                ]
            ]);
        }

        return redirect()->back()->with('status', 'Profile successfully updated.');
    }


    public function activities(Request $request)
    {
        $query = Activity::query();

        switch ($request->get('filter')) {
            case 'downloads':
                $query = $query->where('content_type', 'File');
                break;

            case 'favorites':
                $query = $query->where('content_type', 'Object')->where('description', 'LIKE', '%favorited%');
                break;

            case 'ratings':
                $query = $query->where('content_type', 'Object')->where('description', 'LIKE', '%rated%');
                break;

            case 'others':
                $query = $query->where('content_type', 'User');
                break;

            case 'all':
            default:
                break;
        }

        $activities = $query->where('user_id', Auth::id())->orderBy('id', 'desc')->paginate(20);

        return view('frontend.v2.profile.activities', compact('activities'));
    }

    public function ratings()
    {
        $ratings = Rating::whereAuthorId(Auth::id())->orderBy('id', 'desc')->paginate(20);

        return view('frontend.v2.profile.ratings', compact('ratings'));
    }

    public function favorites()
    {
        $favorites = Favorite::where('user_id', Auth::id())->orderBy('id', 'desc')->paginate(20);

        return view('frontend.v2.profile.favorites', compact('favorites'));
    }
}
