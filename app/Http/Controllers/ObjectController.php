<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Category;
use App\Models\CategoryObject;
use App\Models\File;
use App\Models\FileCategory;
use App\Models\Manufacturer;
use App\Models\Object;
use App\Models\Property;
use App\User;
use Conversion;
use DB;
use Illuminate\Http\Request;
use Trexology\ReviewRateable\Models\Rating;
use Regulus\ActivityLog\Models\Activity;
use Response;

class ObjectController extends Controller
{
    public function index()
    {
        return ['status' => 'ok'];
    }

    public function getBySlug($manufacturerSlug, $objectSlug)
    {
        $manufacturer = Manufacturer::whereSlug($manufacturerSlug)->firstOrFail();
        $object = Object::whereSlug($objectSlug)->firstOrFail();
        
        $rating = 0;
        if (Auth::check()) {
            $rating = Rating::whereReviewrateableType('App\Models\Object')->whereReviewrateableId($object->id)->whereAuthorId(Auth::id())->first();
            if ($rating) {
                $rating = $rating->rating;
            }
        }

        $properties = Property::pluck('display_name', 'name');

        return view('frontend.v2.home.object', compact('manufacturer', 'object', 'rating', 'properties'));
    }

    public function favorite($id)
    {
        if (Auth::check()) {
            $object = Object::findOrFail($id);

            $description = 'favorited_item';
            if ($object->isFavorited()) {
                $object->removeFavorite();
                $description = 'unfavorited_item';
            } else {
                $object->addFavorite();
            }

            Activity::log([
                'contentId'   => $id,
                'contentType' => 'Object',
                'description' => [
                    "$description", [
                        'number' => 1,
                        'item'  => 'SPAL|labels.object'
                    ]
                ]
            ]);

            return ['status' => 'ok'];
        }

        return ['status' => 'fail'];
    }

    public function rate($id, Request $request)
    {
        if (Auth::check()) {
            $user = Auth::user();
            $object = Object::findOrFail($id);

            $rating = Rating::whereReviewrateableType('App\Models\Object')->whereReviewrateableId($id)->whereAuthorId($user->id)->first();

            if (!$rating) {
                $rating = $object->rating([
                    'rating' => $request->get('rating')
                ], $user);
            } else {
                $rating = $object->updateRating($rating->id, [
                    'rating' => $request->get('rating')
                ]);
            }

            Activity::log([
                'contentId'   => $id,
                'contentType' => 'Object',
                'description' => [
                    'rated_item', [
                        'number' => 1,
                        'item'  => 'SPAL|labels.object'
                    ]
                ]
            ]);

            return ['status' => 'ok'];
        }

        return ['status' => 'fail'];
    }

    public function download($id)
    {
        $file = File::findOrFail($id);

        $file_path = storage_path('app/public/' . $file->path);

        if (file_exists($file_path))
        {
            $file->incrementDownloadCount();

            return response()->download($file_path);
        } else {
            exit('Requested file does not exist!');
        }
    }

    public function latest()
    {
        $rawObjects = Object::whereNotIn('id', [830])->whereStatus('PUBLISHED')->orderBy('created_at', 'DESC')->take(3)->get();

        $objects = [];
        foreach ($rawObjects as $rawObject) {
            $object = new \stdClass();
            $object->id = $rawObject->id;
            $object->name = $rawObject->name;
            $object->link = action('ObjectController@getBySlug', [$rawObject->manufacturer->slug, $rawObject->slug]);
            $object->code = $rawObject->code;
            $object->image = url('storage/' . $rawObject->image->first()->path);
            $objects[] = $object;
        }

        return $objects;
    }

    public function mostDownloaded()
    {
        $rawObjects = Object::whereNotIn('id', [830])->whereStatus('PUBLISHED')->inRandomOrder()->take(3)->get();

        $objects = [];
        foreach ($rawObjects as $rawObject) {
            $object = new \stdClass();
            $object->id = $rawObject->id;
            $object->name = $rawObject->name;
            $object->link = action('ObjectController@getBySlug', [$rawObject->manufacturer->slug, $rawObject->slug]);
            $object->code = $rawObject->code;
            $object->image = url('storage/' . $rawObject->image->first()->path);
            $objects[] = $object;
        }

        return $objects;        
    } 
    
    public function mostViewed()
    {
        $rawObjects = Object::whereNotIn('id', [830])->whereStatus('PUBLISHED')->inRandomOrder()->take(3)->get();

        $objects = [];
        foreach ($rawObjects as $rawObject) {
            $object = new \stdClass();
            $object->id = $rawObject->id;
            $object->name = $rawObject->name;
            $object->link = action('ObjectController@getBySlug', [$rawObject->manufacturer->slug, $rawObject->slug]);
            $object->code = $rawObject->code;
            $object->image = url('storage/' . $rawObject->image->first()->path);
            $objects[] = $object;
        }

        return $objects;        
    }   

    public function mostRated()
    {
        $rawObjects = Object::whereNotIn('id', [830])->whereStatus('PUBLISHED')->inRandomOrder()->take(3)->get();

        $objects = [];
        foreach ($rawObjects as $rawObject) {
            $object = new \stdClass();
            $object->id = $rawObject->id;
            $object->name = $rawObject->name;
            $object->link = action('ObjectController@getBySlug', [$rawObject->manufacturer->slug, $rawObject->slug]);
            $object->code = $rawObject->code;
            $object->image = url('storage/' . $rawObject->image->first()->path);
            $objects[] = $object;
        }

        return $objects;
    }

    public function plugin()
    {
        $plugin = Object::find(830);

        $rawFiles = $plugin->files;

        $files = [];
        foreach ($rawFiles as $rawFile) {
            $file = new \stdClass();
            $file->category = $rawFile->categories->first()->name;
            $file->size = Conversion::convert($rawFile->size, 'byte')->to('megabyte') . ' MB';
            $file->download_count = (int) number_format($rawFile->download_count);
            $file->is_downloadable = false;                                
            $file->download_link = '<a data-toggle="tooltip" data-placement="left" title="Please login to download" role="button">Download <i class="fa fa-download" aria-hidden="true"></i></a>';
            if (Auth::check()) {
                $file->is_downloadable = true;
                $file->download_link = '<a href="' . action('ObjectController@download', $rawFile->id) . '" role="button">Download <i class="fa fa-download" aria-hidden="true"></i></a>';
            }
            $files[] = $file;
        }

        return $files;
    }

    public function session()
    {
        $rawUser = Auth::user();

        if (Auth::check()) {
            $user = new \stdClass();
            $user->is_user = true;
            $user->id = $rawUser->id;
            $user->name = $rawUser->name;
        } else {
            $user = new \stdClass();
            $user->is_user = false;
        }

        return Response::json($user);
    }
}