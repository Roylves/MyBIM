<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Category;
use App\Models\CategoryObject;
use App\Models\File;
use App\Models\FileCategory;
use App\Models\Manufacturer;
use App\Models\Object;
use DB;
use Illuminate\Http\Request;
use Trexology\ReviewRateable\Models\Rating;

class HomeController extends Controller
{
    public function migrate()
    {
        // $objects = Object::all();
        $objects = Object::take(500)->skip(3605)->get();

        foreach ($objects as $object) {
            $object->slug = null;
            $object->update(['name' => $object->name, 'status' => 'PUBLISHED']);

            if (file_exists(public_path() . '/' . $object->imageX)) {
                $image =  new File();
                $image->object_id = $object->id;
                $image->path = $object->imageX;
                $image->type = 'IMAGE';
                $image->save();
            }
            
            $properties = json_decode($object->properties);

            $path = str_replace('png', 'stl', $object->imageX);
            $path = str_replace('-' .$properties->length . 'M', '', $path);
            if (file_exists(public_path() . '/' . $path)) {
                $stl =  new File();
                $stl->object_id = $object->id;
                $stl->path = $path;
                $stl->type = 'STL';
                $stl->save();
            }

            $path = str_replace('png', 'dwg', $object->imageX);
            $path = str_replace('-' .$properties->length . 'M', '', $path);
            if (file_exists(public_path() . '/' . $path)) {
                $dwg =  new File();
                $dwg->object_id = $object->id;
                $dwg->path = $path;
                $dwg->type = 'PLUGIN';
                $dwg->save();
            }

            $path = str_replace('png', 'ifc', $object->imageX);
            $path = str_replace('-' .$properties->length . 'M', '', $path);
            if (file_exists(public_path() . '/' . $path)) {
                $ifc =  new File();
                $ifc->object_id = $object->id;
                $ifc->path = $path;
                $ifc->type = 'PLUGIN';
                $ifc->save();
            }

            $path = str_replace('png', 'rfa', $object->imageX);
            $path = str_replace('-' .$properties->length . 'M', '', $path);
            if (file_exists(public_path() . '/' . $path)) {            
                $rfa =  new File();
                $rfa->object_id = $object->id;
                $rfa->path = $path;
                $rfa->type = 'PLUGIN';
                $rfa->save();
            }
            
            $path = str_replace('png', 'rvt', $object->imageX);
            $path = str_replace('-' .$properties->length . 'M', '', $path);
            if (file_exists(public_path() . '/' . $path)) {            
                $rvt =  new File();
                $rvt->object_id = $object->id;
                $rvt->path = $path;
                $rvt->type = 'PLUGIN';
                $rvt->save();
            }

            $path = str_replace('png', 'zip', $object->imageX);
            $path = str_replace('-' .$properties->length . 'M', '', $path);
            if (file_exists(public_path() . '/' . $path)) {
                $zip =  new File();
                $zip->object_id = $object->id;
                $zip->path = $path;
                $zip->type = 'PLUGIN';
                $zip->save();
            }

        //     // dwg
        //     // ifc
        //     // png
        //     // rfa
        //     // rvt
        //     // stl
        //     // zip

        }
        dd();
    }

    public function migrate2()
    {
        $string = 'cidb_assets/ibs/jkr/cr/0m/png/CR-3060-C25-0M.png';
        //dd(explode('/', $string));
        $code = 'CR-3060-C25';
        // $level1 = explode('-', $code)[0];
        // $length = 
        // dd();
        // $newObject = Object::findOrFail(1);
        // $newObject2 = \App\Models\Object2::where('code', $newObject->code)->firstOrFail();
        // // dd($newObject);

        // $path = str_replace('cidb_assets/ibs/', '', $newObject2->imageX);
        // $path = public_path() . '/storage/objects/' . $path;
        // // dd($path);
        // // dd(file_exists($path));
        // if (file_exists(public_path() . '/' . $path)) {
        //     $stl =  new File();
        //     $stl->object_id = $object->id;
        //     $stl->path = $path;
        //     $stl->type = 'STL';
        //     $stl->save();
        // }


        // // dd($newObject->properties['length']);
        // // dd(public_path('storage/objects/cidb/br/3m/png/BR250-600-04-3M.png'));
        // dd(file_exists(public_path("storage/objects/{$newObject->manufacturer->slug}/br/{$newObject->properties['length']}m/png/{$newObject->code}-{{$newObject->properties['length']}M.png")));

        // // FILES
        // if (file_exists(public_path('cidb/br/3m/png/BR250-600-04-3M.png'))) {
        //     $image =  new File();
        //     $image->object_id = $object->id;
        //     $image->path = $object->imageX;
        //     $image->type = 'IMAGE';
        //     $image->save();
        // }

        \Excel::filter('chunk')->load('bim.csv')->chunk(250, function($results)
        {
            foreach($results as $row)
            {
                $newObject = new Object();
                $manufacturer = Manufacturer::where('name', $row['catalogue'])->firstOrFail();
                $newObject->manufacturer_id = $manufacturer->id;
                $newObject->name = $row['name'];
                // $newObject->slug = $row['name'];
                $newObject->code = $row['code'];
                // $newObject->system_code = $row['name'];
                $newObject->description = $row['description'];
                $property = [
                    'length' => $row['length'],
                    'width' => $row['width'],
                    'height' => $row['height'],
                    'moment' => $row['moment'],
                    'min_moment' => $row['min_moment'],
                    'shear' => $row['shear'],
                    'axial_load' => $row['axial_load'],
                    'min_axial_load' => $row['min_axial_load'],
                    'self_weight' => $row['self_weight']
                ];

                // $property = new \stdClass();
                // $property->length = $row['length'];
                // $property->width = $row['width'];
                // $property->height = $row['height'];
                // $property->moment = $row['moment'];
                // $property->min_moment = $row['min_moment'];
                // $property->shear = $row['shear'];
                // $property->axial_load = $row['axial_load'];
                // $property->min_axial_load = $row['min_axial_load'];
                // $property->self_weight = $row['self_weight'];

                $newObject->properties = $property;
                $newObject->status = 'PUBLISHED';
                $newObject->save();

                // PRECAST
                $newCategoryObject = new CategoryObject();
                $newCategoryObject->category_id = 6; // IBS
                $newCategoryObject->object_id = $newObject->id;
                $newCategoryObject->save();

                // CATALOGUE
                $newCategoryObject = new CategoryObject();
                $category = Category::where('name', $row['catalogue'])->firstOrFail();
                $newCategoryObject->category_id = $category->id;
                $newCategoryObject->object_id = $newObject->id;
                $newCategoryObject->save();

                // COMPONENT
                $newCategoryObject = new CategoryObject();
                $category = Category::where('name', $row['component'])->firstOrFail();
                $newCategoryObject->category_id = $category->id;
                $newCategoryObject->object_id = $newObject->id;
                $newCategoryObject->save();

                // TYPE
                $newCategoryObject = new CategoryObject();
                $category = Category::where('name', $row['type'])->firstOrFail();
                $newCategoryObject->category_id = $category->id;
                $newCategoryObject->object_id = $newObject->id;
                $newCategoryObject->save();
                
                $properties = $newObject->properties;
                // dd($properties);
                
                $prefix = explode('-', $newObject->code);
                $prefix = strtolower(preg_replace('/[0-9]+/', '', $prefix[0]));
                $length = $properties['length'] . 'm';
                $suffix = '-' . $properties['length'] . 'M';
                // dd($path);
                
                $extensions = [
                    'png' => 'IMAGE',
                    'stl' => 'STL',
                    'dwg' => 'PLUGIN',
                    'ifc' => 'PLUGIN',
                    'rfa' => 'PLUGIN', 
                    'rvt' => 'PLUGIN',
                    'zip' => 'PLUGIN',
                ];

                foreach ($extensions as $extension => $type) {
                    if ($extension == 'png') {
                        $path = "objects/{$newObject->manufacturer->slug}/{$prefix}/{$length}/{$extension}/{$newObject->code}{$suffix}.{$extension}"; 
                    } else {
                        $path = "objects/{$newObject->manufacturer->slug}/{$prefix}/{$length}/{$extension}/{$newObject->code}.{$extension}"; 
                    }

                    $storedPath = $path;
                    $checkPath = public_path('storage/' . $path);
                    // dd($checkPath);
                    // dd(file_exists($checkPath));
                    if (file_exists($checkPath)) {
                        $stl =  new File();
                        $stl->manufacturer_id = $newObject->manufacturer_id;
                        $stl->object_id = $newObject->id;
                        $stl->path = $storedPath;
                        $mime = \File::mimeType($checkPath);
                        $size = \File::size($checkPath);
                        $stl->mime = $mime;
                        $stl->size = $size;
                        $stl->type = $type;
                        $stl->save();
                    } else {
                        //echo 'IMAGE NOT FOUND FOR' . $newObject->id;
                        //dd();
                        \Log::info("{$row['catalogue']} - {$extension} - $newObject->id,$newObject->code," . $properties['length']);
                    }
                }
            }
        });
    }

    public function index(Request $request)
    {
        $inactiveManufacturersIds = [];
        $inactiveObjectCategoriesIds = [];
        $inactiveFileCategoriesIds = [];
        
        $rawManufacturers = Manufacturer::all();
        foreach ($rawManufacturers as $manufacturer) {
            if ($manufacturer->status == Manufacturer::STATUS_PUBLISHED) {
                $manufacturers[] = $manufacturer;
            } else {
                $inactiveManufacturersIds[] = $manufacturer->id;
            }
        }

        $rawObjectCategories = Category::where('name', '!=', 'Plugin')->whereNull('parent_id')->get();
        foreach ($rawObjectCategories as $objectCategory) {
            if ($objectCategory->status == Category::STATUS_PUBLISHED) {
                $objectCategories[] = $objectCategory;
            } else {
                $inactiveObjectCategoriesIds[] = $objectCategory->id;
            }
        }

        $rawFileCategories = FileCategory::where('name', '!=', 'Plugin')->whereNull('parent_id')->get();
        $fileCategories = [];
        foreach ($rawFileCategories as $fileCategory) {
            if ($fileCategory->status == Category::STATUS_PUBLISHED) {
                $fileCategories[] = $fileCategory;
            } else {
                $inactiveFileCategoriesIds[] = $fileCategory->id;
            }
        }

        $objectQuery = Object::query();

        if ($request->has('keyword')) {
            $objectQuery->where(function($q) use ($request) {
                $q->where(function($query) use ($request){
                    $query->where('name', 'like', '%' . $request->get('keyword') . '%');
                });
                $q->orWhere(function($query) use ($request) {
                    $query->where('code', 'like', '%' . $request->get('keyword') . '%');
                });
                $q->orWhere(function($query) use ($request) {
                    $query->where('description', 'like', '%' . $request->get('keyword') . '%');
                });
            });
        }

        // don't include Manufacturer with Draf/Pending statuses
        if ($inactiveManufacturersIds) {
            $objectQuery->whereNotIn('manufacturer_id', $inactiveManufacturersIds);
        }

        if ($request->has('manufacturer_id')) {  
            $objectQuery->whereIn('manufacturer_id', $request->get('manufacturer_id'));
        }

        // don't include Plugin category
        $objectQuery->whereHas('categories', function($query) use ($request)
        {
            $query->where('category_id', '!=', 1);
        });

        // don't include Category with Draf/Pending statuses
        $objectQuery->whereHas('categories', function($query) use ($request, $inactiveObjectCategoriesIds)
        {
            $query->whereNotIn('category_id', $inactiveObjectCategoriesIds);
        });

        if ($request->has('object_category_id')) {
            foreach ($request->get('object_category_id') as $key => $category_id) {
                $objectQuery->whereHas('categories', function($query) use ($key, $request)
                {
                    $query->where('category_id', $request->get('object_category_id')[$key]);
                });
            }
        }

        if ($request->has('file_category_id')) {
            $objectQuery->whereHas('files', function($query) use ($request)
            {
                $query->whereHas('categories', function($query) use ($request)
                {
                    foreach ($request->get('file_category_id') as $category_id) {
                        $query->where('category_id', $category_id);
                    }
                });
            });
        }

        $objects = $objectQuery->published()->orderBy('featured', 'DESC')->paginate(6);

        return view('frontend.v2.home.index', compact('manufacturers', 'objectCategories', 'fileCategories', 'objects'));
    }

    public function manufacturer($slug)
    {
        $manufacturer = Manufacturer::whereSlug($slug)->firstOrFail();

        return view('frontend.home.manufacturer', compact('manufacturer'));
    } 

    public function plugin()
    {
        $categoryObject = CategoryObject::join('categories', 'categories.id', '=', 'category_object.category_id')
            ->join('objects', 'objects.id', '=', 'category_object.object_id')
            ->where('categories.name', 'Plugin')
            ->where('objects.status', Object::STATUS_PUBLISHED)
            ->first();

        $object = null;
        $featured = null;
        if ($categoryObject !== null) {
            $object = Object::findOrFail($categoryObject->object_id);
            $featured = File::where('object_id', $object->id)->featured()->first();
            if ($featured === null) {
                $featured = $object->files->first();
            }
        }

        return view('frontend.home.plugin', compact('object', 'featured'));
    }
}